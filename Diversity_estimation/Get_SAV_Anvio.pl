#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Bio::Seq;
use Bio::SeqUtils;
my $h='';
my $cmd='';
my $wdir='';
my $dgr_code='';
my $merged_reads_1='';
my $path_freebayes='';
GetOptions ('help' => \$h, 'h' => \$h, 'd=s'=>\$wdir, 'i=s'=>\$dgr_code, 'r=s'=>\$merged_reads_1, 'f=s'=>\$path_freebayes);
if ($h==1 || $wdir eq "" || $dgr_code eq "" || $merged_reads_1 eq ''){ # If asked for help or did not set up any argument
	print "# Script to identify SAVs around a DGR locus using Anvi'o
# Arguments :
# -d: working directory
# -i: DGR code
# -r: read file R1 (fastq.gz)
";
	die "\n";
}
if (!($wdir=~/\/$/)){$wdir.="/";}
&test_avail("anvi-gen-contigs-database");

## Setup everything
my $info_file=$wdir.$dgr_code."_info.tsv";
my $bam_dir=$wdir.$dgr_code."/";
if (!(-d $bam_dir)){print "#$#$# pblm $bam_dir doesn't exist\n"; next;}
my $ref_fna=$wdir.$dgr_code."_selected.fna";
my $gff_file=$wdir.$dgr_code."_selected.gff";
if (!(-e $ref_fna)){die("#$#$# Pblm, we don't find $ref_fna\n");}
if (!(-e $gff_file)){die("#$#$# Pblm, we don't find $gff_file\n");}
$merged_reads_1=~/.*?([^\/]+)_R1\.fastq.gz/;
my $taxon_id=$1;
my $bam_sorted=$bam_dir.$taxon_id."_cover50_globali_sorted.bam";

my %aa_decode=&get_aa_decode();


### Take info on contig
print "### Take info on contig and check sufficient depth \n";
my %info_contig;
my %check_target;
open my $tsv,"<",$info_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	$info_contig{$tab[0]}=$tab[1];
	if ($tab[0]=~/^Target_gene/){$check_target{$tab[0]}=1;}
}
close $tsv;
my $l=$info_contig{"Selected_end"}-$info_contig{"Selected_start"};

my $ref_seq="";
open my $fna,"<",$ref_fna;
while(<$fna>){
	chomp($_);
	if ($_=~/^>/){}
	else{
		$ref_seq.=$_;
	}
}
close $fna;


print "### Create the Anvi'o input file if needed \n";
my $anvio_gene_call=$bam_dir.$dgr_code."_selected_gene_calls.txt";
if (!(-e $anvio_gene_call)){
	&generate_anvio_files($gff_file,$dgr_code,$bam_dir);
	if (!(-e $anvio_gene_call)){die("Pblm generating input files for anvio, we stop there -- $anvio_gene_call\n");}
}
## load id of genes of interest
my %translate_gene;
my %rev_translate;
my $translate_file=$bam_dir.$dgr_code."_selected_translate_gene_anvio.txt";
open my $tsv,"<",$translate_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	$translate_gene{$tab[0]}=$tab[1];
	$rev_translate{$tab[1]}=$tab[0];
}
close $tsv;
## load coordinates of each target gene
my %info_gene;
open my $tsv,"<",$anvio_gene_call;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($tab[0] eq "gene_callers_id"){}
	else{
		my $gene=$rev_translate{$tab[0]};
		$info_gene{$gene}{"start"}=$tab[2]+1;
		$info_gene{$gene}{"stop"}=$tab[3];
		$info_gene{$gene}{"strand"}=$tab[4];
		if (($info_gene{$gene}{"stop"}-$info_gene{$gene}{"start"}+1)%3==0){}
		else{
# 			print "?! Pblm with $gene ? ".$info_gene{$gene}{"start"}." // ".$info_gene{$gene}{"stop"}."\n";
		}
	}
}
close $tsv;


## Create anvio contigs database if needed
my $anvio_contigs_db=$bam_dir.$dgr_code."_contig.db";
if (-e $anvio_contigs_db){
	print "$anvio_contigs_db is already here, we'll use it\n";
}
else{
	&run_cmd("anvi-gen-contigs-database -f $ref_fna -o $anvio_contigs_db -L 0 --external-gene-calls $anvio_gene_call --project-name '$dgr_code'");
	if (!(-e $anvio_contigs_db)){die("$anvio_contigs_db is missing, we need it before we can move on, please double-check anvio database\n");}
}
## Create the Anvi'o profile if needed for each dataset, then the merged one
print "### Create the Anvi'o profile if needed \n";
my %store_coverage;
my $store_profiles="";

#### NOTE -> THIS IS A SINGLE METAGENOME NOW, SO CAN LIKELY BE SIMPLIFIED

print "### Process $taxon_id \n";
my $profile=$bam_dir.$taxon_id."_cover50_globali_sorted.bam-ANVIO_PROFILE/PROFILE.db";
if (-e $profile){
	print "$profile already here, all good\n";
	$store_profiles=$profile;
	### Load coverage
	my $cover_file=$bam_dir.$taxon_id."_cover50_globali_sorted.bam.cover";
	if (!(-e $cover_file)){die("bam file $cover_file is missing, we need it before we can move on\n");}
	my $tmp_taxon_id=$taxon_id;
	$tmp_taxon_id=~tr/[a-z]/[A-Z]/; ## Anvio switches everything to upper-case
	open my $tsv,"<",$cover_file;
	while(<$tsv>){
		chomp($_);
		my @t=split("\t",$_);
		$store_coverage{$tmp_taxon_id}{$t[1]}=$t[2];
	}
	close $tsv;
}
else{
	my $bam_file_filtered=$bam_dir.$taxon_id."_cover50.bam";
	my $test=&test_filtered($bam_file_filtered);
	if ($test==0){
		print "Filtered bam file is empty, we stop there\n";
		next;
	}
	### sorted file
	my $bam_sorted=$bam_file_filtered;
	$bam_sorted=~s/\.bam/_globali_sorted.bam/;
	### Check that we have enough depth
	my $cover_file=$bam_sorted.".cover";
	if (!(-e $cover_file)){die("bam file $cover_file is missing, we need it before we can move on -- $bam_file_filtered\n");}
	my $tmp_taxon_id=$taxon_id;
	$tmp_taxon_id=~tr/[a-z]/[A-Z]/; ## Anvio switches everything to upper-case
	open my $tsv,"<",$cover_file;
	my $n=0;
	my $total=0;
	while(<$tsv>){
		chomp($_);
		my @t=split("\t",$_);
		# $t[1] = position, $t[2] = coverage
		$total+=$t[2];
		$store_coverage{$tmp_taxon_id}{$t[1]}=$t[2];
		$n++;
	}
	close $tsv;
	my $ratio=$n/$l;
	my $avg=$total/$l;
	if ($avg<2){
		print "Average coverage is $avg, we stop there\n";
	}
	else{
		print "$taxon_id has $avg coverage, so we create an ANVIO profile for it\n";
		# Check that we have the input
		if (!(-e $bam_sorted)){die("bam file $bam_sorted is missing, we need it before we can move on\n");}
		&run_cmd("anvi-profile -i $bam_sorted -c $anvio_contigs_db --report-variability-full --profile-SCVs");
		if (!(-e $profile)){
			print "$profile was not created, we stop there\n";
			die("dead\n");
		}
		else{
			print "Adding $profile to the list\n";
			$store_profiles=$profile;
		}
	}
}

if ($store_profiles eq ""){
	die("We stop there, there were no profiles created\n");
}

# Making an output file for each target
my $out_file=$bam_dir.$dgr_code."_anvio_SAV.csv";
my $empty_file=$bam_dir.$dgr_code."_anvio_SAV.empty";
if (-e $empty_file){
	print "We already tried this one, and there's no SNP in the Target gene, so we can't do much\n";
	die("\n");
}
open my $s1,">",$out_file;
print $s1 "Target,Target_gene,Sample,Pos_codon,Position,Type,Coverage,Consensus,Entropy,Coverage_consensus,Full_vector\n";
print "### Creating matrices for each target\n";
foreach my $target (sort {$a cmp $b} keys %check_target){
	my $target_gene=$info_contig{$target};
	$target=~/.*_(\d+)$/;
	my $repeat_code="Repeat_".$1;
	my $vr_code="VR_shifted_".$1;
	my $tr_seq_code="TR_seq_".$1;
	my $vr_seq_code="VR_seq_".$1;
	my $matrix_file=$bam_dir.$dgr_code."_".$target_gene."_aa_matrix.tsv";
	my $translated_id="";
	my $vr_shifted=$info_contig{$vr_code};
	my @t_test=split(":",$vr_shifted);
	if ($info_gene{$target_gene}{"start"}>=$t_test[0]){
		### Have to adjust vr shifted coordinates because we actually only took the sequences within the predicted CDS
		$t_test[0]=$info_gene{$target_gene}{"start"};
		$vr_shifted=$t_test[0].";".$t_test[1];
	}
	if ($info_gene{$target_gene}{"stop"}<=$t_test[1]){
		### Have to adjust vr shifted coordinates because we actually only took the sequences within the predicted CDS
		$t_test[1]=$info_gene{$target_gene}{"stop"};
		$vr_shifted=$t_test[0].";".$t_test[1];
	}


	my $tr_seq=$info_contig{$tr_seq_code};
	my $vr_seq=$info_contig{$vr_seq_code};
	print "$target - $target_gene - $vr_shifted - $tr_seq - $vr_seq\n";
	$translated_id=$translate_gene{$target_gene};
	if ($translated_id eq ""){die("We never found the translation of $target_gene, we stop there\n");}
	if (-e $matrix_file){
		print "We use the existing $matrix_file\n";
	}
	else{
		my $matrix_file_nucl=$bam_dir.$dgr_code."_".$target_gene."_nucl_matrix.tsv";
		my $matrix_file_cdn=$bam_dir.$dgr_code."_".$target_gene."_cdn_matrix.tsv";
		&run_cmd("anvi-gen-variability-profile -c $anvio_contigs_db -p $store_profiles --gene-caller-ids $translated_id -o $matrix_file_nucl");
		&run_cmd("anvi-gen-variability-profile -c $anvio_contigs_db -p $store_profiles --gene-caller-ids $translated_id --engine CDN -o $matrix_file_cdn");
		&run_cmd("anvi-gen-variability-profile -c $anvio_contigs_db -p $store_profiles --gene-caller-ids $translated_id --engine AA -o $matrix_file");
		### NOTE: Below are for cases where we have a merged profile including multiple samples
		# &run_cmd("anvi-gen-variability-profile -c $anvio_contigs_db -p $merged_profile_file --gene-caller-ids $translated_id -o $matrix_file_nucl --quince-mode");
		# &run_cmd("anvi-gen-variability-profile -c $anvio_contigs_db -p $merged_profile_file --gene-caller-ids $translated_id --engine CDN -o $matrix_file_cdn --quince-mode");
		# &run_cmd("anvi-gen-variability-profile -c $anvio_contigs_db -p $merged_profile_file --gene-caller-ids $translated_id --engine AA -o $matrix_file --quince-mode");

	}
	if (!(-e $matrix_file)){
		print "Error during SNP calling, in which case there is no output matrix to Anvio\n";
		close $s1;
		&run_cmd("touch $empty_file");
		&run_cmd("rm $out_file");
		die("Stopping here\n");
	}
	my %store_result_aa=&parse_aa_matrix($matrix_file,$vr_shifted,$tr_seq,$vr_seq,$info_gene{$target_gene}{"start"},$info_gene{$target_gene}{"stop"},$info_gene{$target_gene}{"strand"},$ref_seq);
	## Prep the aa seq to double check that we are looking at the correct position
	my $gene=substr($ref_seq,$info_gene{$target_gene}{"start"}-1,($info_gene{$target_gene}{"stop"}-$info_gene{$target_gene}{"start"}+1));
	my $aa_seq="";
	my $seq_bio = Bio::Seq->new(-id => $target_gene , -seq =>$gene, -alphabet => 'dna', -verbose => -1);
	my @tab_aa= Bio::SeqUtils->translate_6frames($seq_bio, -verbose => -1);
	if ($info_gene{$target_gene}{"strand"} eq "f"){$aa_seq=$tab_aa[0]->seq;}
	elsif ($info_gene{$target_gene}{"strand"} eq "r"){$aa_seq=$tab_aa[3]->seq;}
	my @t_seq_aa=split("",$aa_seq);
	print $aa_seq."\n";

	foreach my $sample (sort {$a <=> $b} keys %store_result_aa){
		foreach my $codon (sort {$a <=> $b} keys %{$store_result_aa{$sample}}){
			if ($store_result_aa{$sample}{$codon}{"coverage"}==0){
				$store_result_aa{$sample}{$codon}{"consensus"}="NA";
			}
			### Double checking a bit
			print "Checking codon $codon -- equal to position --".$store_result_aa{$sample}{$codon}{"position"}."-- in sample --$sample--\n";
			print $sample."\t".$store_result_aa{$sample}{$codon}{"position"}."\t".$store_coverage{$sample}{$store_result_aa{$sample}{$codon}{"position"}}."\n";
			if ($store_result_aa{$sample}{$codon}{"position"}=~/;/){ ## Quick averaging of coverage across codon positions
				my @t=split(";",$store_result_aa{$sample}{$codon}{"position"});
				print "Calculating coverage of ".$store_coverage{$sample}{$store_result_aa{$sample}{$codon}{"position"}}." in $sample\n";
				my $total=0;
				foreach my $tmpposition (@t){
					$total+=$store_coverage{$sample}{$tmpposition};
					print "\t$tmpposition\t$store_coverage{$sample}{$tmpposition}\n";
				}
				$store_coverage{$sample}{$store_result_aa{$sample}{$codon}{"position"}}=$total/(scalar(@t));
				print $store_coverage{$sample}{$store_result_aa{$sample}{$codon}{"position"}}."\n";
			}
			if ($t_seq_aa[$codon] eq "*"){next;} ## Not interested by stop codons
			print "Anvio coverage = ".$store_result_aa{$sample}{$codon}{"coverage"}." vs samtools = ".$store_coverage{$sample}{$store_result_aa{$sample}{$codon}{"position"}}."\n";
			print "Anvio AA ref : ".$store_result_aa{$sample}{$codon}{"reference"}." vs residue in ref sequence: ".$t_seq_aa[$codon]." = ".$aa_decode{"1L"}{$t_seq_aa[$codon]}{"3L"}."\n";
			if ($store_result_aa{$sample}{$codon}{"consensus"} eq "NA"){ ## No info from Anvio, likely because very low coverage or no SNV, so we backfill
				$store_result_aa{$sample}{$codon}{"coverage"}=$store_coverage{$sample}{$store_result_aa{$sample}{$codon}{"position"}};
				$store_result_aa{$sample}{$codon}{"coverage_consensus"}=$store_coverage{$sample}{$store_result_aa{$sample}{$codon}{"position"}};
				$store_result_aa{$sample}{$codon}{"consensus"}=$aa_decode{"1L"}{$t_seq_aa[$codon]}{"3L"};
				$store_result_aa{$sample}{$codon}{"full_vector"}=$store_result_aa{$sample}{$codon}{"consensus"}.":".$store_result_aa{$sample}{$codon}{"coverage_consensus"}.";";
				$store_result_aa{$sample}{$codon}{"entropy"}=0;
				if ($store_result_aa{$sample}{$codon}{"coverage"} eq ""){
					die("Pblm - no coverage for ".$store_result_aa{$sample}{$codon}{"position"}." in sample ".$sample." (codon: ".$codon.")\n");
				}
			}
			elsif ($store_result_aa{$sample}{$codon}{"reference"} ne $aa_decode{"1L"}{$t_seq_aa[$codon]}{"3L"}){
				print "Pblm ? \n";
# 				<STDIN>;
				die("\n");
			}
			print $s1 $repeat_code.",".$target_gene.",".$sample.",".$codon.",".$store_result_aa{$sample}{$codon}{"position"}.",".$store_result_aa{$sample}{$codon}{"type"}.",".
			$store_result_aa{$sample}{$codon}{"coverage"}.",".$store_result_aa{$sample}{$codon}{"consensus"}.",".$store_result_aa{$sample}{$codon}{"entropy"}.",".
			$store_result_aa{$sample}{$codon}{"coverage_consensus"}.",".$store_result_aa{$sample}{$codon}{"full_vector"}."\n";
		}
	}
}
close $s1;

## Functions
sub parse_aa_matrix{
	## Input needed - in matrix from anvio, coordinates of the VR, list of samples
	my $in_matrix=$_[0];
	my $vr=$_[1];
	my $tr_seq=$_[2];
	my $vr_seq=$_[3];
	my $start=$_[4];
	my $stop=$_[5];
	my $strand=$_[6];
	my $ref_seq=$_[7];

	my $nucl_matrix=$_[0];
	$nucl_matrix=~s/_aa_matrix.tsv/_nucl_matrix.tsv/;


	$tr_seq=~tr/atcgn/ATCGN/;
	$vr_seq=~tr/atcgn/ATCGN/;

	## Only take changes in the VR
	print "Gene from $start to $stop on $strand, with vr in $vr\n";
	my @s_ref=split("",$ref_seq);
	my @t=split(":",$vr);
	if ($strand eq "r"){
		## Have to reverse the actual sequences, otherwise we are not getting the correct position
		$tr_seq=reverse($tr_seq);
		$vr_seq=reverse($vr_seq);
		print "!!! THIS IS A REVERSE -> We reverse the sequences\n";
	}

	my @t_seq=split("",$tr_seq);
	my @v_seq=split("",$vr_seq);
	my %check_position;
	my %check_codon;
	my %store;
	my $min_codon=9999;
	my $n_gap=0;
	for (my $i=0;$i<=$#t_seq;$i++){
		if ($v_seq[$i] eq "-"){
			$n_gap++;
		}
		my $pos=$t[0]+$i-$n_gap;
		if ($t_seq[$i] eq "A" || $t_seq[$i] eq "a"){
			print "Position $pos is ".$t_seq[$i]." in the TR, we are interested\n";
			$check_position{$pos}{"sequence_TR"}=$t_seq[$i];
			$check_position{$pos}{"sequence_VR"}=$v_seq[$i];
			if ($v_seq[$i] eq ""){
				print "$i == $v_seq[$i] in v_seq ???\n";
				print join("",@v_seq)."\n";
				<STDIN>;
			}
			if ($strand eq "f"){
				my $codon=int(($pos-$start)/3);
				print "\t corresponds to codon $codon\n";
				$check_position{$pos}{"calculated_codon"}=$codon;
				if ($codon<$min_codon){$min_codon=$codon;}
			}
			elsif ($strand eq "r"){
				my $codon=int(($stop-$pos)/3);
				print "\t corresponds to codon $codon\n";
				$check_position{$pos}{"calculated_codon"}=$codon;
				if ($codon<$min_codon){$min_codon=$codon;}
			}
		}
	}
	## Load the correspondance between position in Anvi'o and Codon in Anvi'o
	my %translate_pos;
	my %seen;
	my %col_name;
	open my $tsv,"<",$nucl_matrix;
	while(<$tsv>){
		chomp($_);
		my @tab=split("\t",$_);
		if ($tab[0] eq "entry_id"){
			for (my $i=0;$i<=$#tab;$i++){$col_name{$i}=$tab[$i];}
		}
		else{
			my $pos=$tab[2]+1;
# 			my $pos=$tab[2];
			my $pos_in_codon=$tab[8];
			my $pos_codon=$tab[9];
			my $ref=$tab[20];
			if ($strand eq "r"){$ref=&revcomp($ref);}
			my $consensus=$tab[21];
			if ($strand eq "r"){$consensus=&revcomp($consensus);}
			if (defined($check_position{$pos}) && !defined($seen{$pos})){
				$seen{$pos}=1;
# 				print $pos."\t".$pos_codon."\t".$tab[20]."\t".$ref."\t".$consensus."\n";
# 				print join("\t",@tab)."\n";
				print $pos."\t".$s_ref[$pos-1]." - Checking in the actual whole sequence\n"; ## -1 because perl starting at 0
# 				<STDIN>;
				$translate_pos{$pos}{"pos_codon"}=$pos_codon;
				$translate_pos{$pos}{"pos_in_codon"}=$pos_in_codon;
				print "$pos -> $pos_in_codon in codon $pos_codon / we previously calculated ".$check_position{$pos}{"calculated_codon"}."\n\t --- REF: $ref vs ".$check_position{$pos}{"sequence_VR"}." while in TR -> ".$check_position{$pos}{"sequence_TR"}."\n";
				if ($ref ne $check_position{$pos}{"sequence_VR"}){
					print " !!! Pblm - inconsistency between position in ref from Anvio and position in SNP\n";
					die("\n");
				}
				if ($pos_codon != $check_position{$pos}{"calculated_codon"}){
					print " !!! We thought that $pos was linked to ".$check_position{$pos}{"calculated_codon"}." but Anvi'o tells us it should be $pos_codon ??\n";
					die("\n");
				}
				$check_codon{$pos_codon}{"pos"}.=$pos.";";
				$check_codon{$pos_codon}{"type"}="VR_A";
# 				<STDIN>;
			}
		}
	}
	close $tsv;

	foreach my $pos (keys %check_position){
		if (!defined($seen{$pos})){
			print "$pos was not found, we use the back-calculated way to check the codon\n";
			$check_codon{$check_position{$pos}{"calculated_codon"}}{"pos"}.=$pos.";";
		}
	}

	print "Now getting 10 random AA before $min_codon - 10\n";
	my $n=10;
	if ($min_codon < 50){$n=0;} ## Only if you actually have codons before
	while($n>0){
		my $selected_codon=int(rand()*($min_codon-10));
		print $selected_codon."\n";
		while ($check_codon{$selected_codon}{"type"} eq "Random_nonVR_A"){
			$selected_codon=int(rand()*($min_codon-10));
			print $selected_codon."\n";
		}
		$check_codon{$selected_codon}{"type"}="Random_nonVR_A";
		if ($strand eq "f"){
			my $pos=(3*$selected_codon)+$start;
			$check_codon{$selected_codon}{"pos"}=$pos;
			# my $codon=int(($pos-$start)/3);
		}
		elsif($strand eq "r"){
			my $pos=$stop-(3*$selected_codon);
			$check_codon{$selected_codon}{"pos"}=$pos;
			# my $codon=int(($stop-$pos)/3);
		}
		print "we took $selected_codon -> ".$check_codon{$selected_codon}{"type"}." // ".$check_codon{$selected_codon}{"pos"}."\n";
		$n--;
	}



# 	<STDIN>;
	## output a clean matrix with entropy and consensus AA at each position for each sample
	%col_name=();
	my %count_summary;
	my %check_sample;
	$count_summary{"VR_A"}{"high_entropy"}=0;
	$count_summary{"VR_A"}{"low_entropy"}=0;
	$count_summary{"non_VR_A"}{"high_entropy"}=0;
	$count_summary{"non_VR_A"}{"low_entropy"}=0;
	open my $tsv,"<",$in_matrix;
	while(<$tsv>){
		chomp($_);
		my @tab=split("\t",$_);
		if ($tab[0] eq "entry_id"){
			for (my $i=0;$i<=$#tab;$i++){$col_name{$i}=$tab[$i];}
			if ($tab[8] eq "Ala"){}
			else{
				print "!!! PBLM WITH $tab[8] -> Should be Ala (We are in $in_matrix)\n";
				<STDIN>;
			}
			if ($tab[28] eq "Val"){}
			else{
				print "!!! PBLM WITH $tab[28] -> Should be Val (We are in $in_matrix)\n";
			}
		}
		else{
			my $cover=$tab[7];
			my $pos_codon=$tab[4];
# 			print $pos_codon."\t".$tab[35]."\t".$tab[30]."\n";
			my $sample=$tab[2];
			$sample=~s/^s//;
			$sample=~s/_COVER50_GLOBALI_SORTED$//;
			$check_sample{$sample}=1;
# 			print $tab[2]."\t".$sample."\n";<STDIN>;
			if (defined($check_codon{$pos_codon})){
				print "\twe like the codon $pos_codon -- $check_codon{$pos_codon}{pos} -- $check_codon{$pos_codon}{type}\n";
				print "\t\tEntropy of $tab[35]\n";
				print "\t\tConsensus is $tab[30]\n";
				if (defined($store{$sample}{$pos_codon})){
					print "$sample - $pos_codon already has something stored ??\n";
					<STDIN>;
				}
				else{
					if ($check_codon{$pos_codon}{"pos"}=~/;$/){chop($check_codon{$pos_codon}{"pos"});}
					$store{$sample}{$pos_codon}{"position"}=$check_codon{$pos_codon}{"pos"};
					$store{$sample}{$pos_codon}{"coverage"}=$cover;
					$store{$sample}{$pos_codon}{"reference"}=$tab[29];
					$store{$sample}{$pos_codon}{"consensus"}=$tab[30];
					$store{$sample}{$pos_codon}{"entropy"}=$tab[35];
					my $departure_from_consensus=$tab[33];
					$store{$sample}{$pos_codon}{"coverage_consensus"}=$cover*(1-$tab[33]);
					if ($tab[29] ne $tab[30]){
						print "Reference = $tab[29], consensus = $tab[30]\n";
# 						<STDIN>;
					}
					#### Take the whole vector of AA as well
					for (my $i=8;$i<=28;$i++){
						# print $i."\t".$col_name{$i}."\n";
						if ($tab[$i]>0){
							$store{$sample}{$pos_codon}{"full_vector"}.=$col_name{$i}.":".$tab[$i].";";
						}
					}
					# <STDIN>;
				}
				if ($check_codon{$pos_codon}{"type"} eq "Random_nonVR_A"){
					$store{$sample}{$pos_codon}{"type"}="Background";
					if ($tab[35]>=0.25){
						$count_summary{"non_VR_A"}{"high_entropy"}++;
	# 					print "\t\t$pos_codon\t$tab[35]\n";
	# 					<STDIN>;
					}
					else{
						$count_summary{"non_VR_A"}{"low_entropy"}++;
					}
				}
				else{
					print "$pos_codon -> ".$check_codon{$pos_codon}{"type"}."/".$check_codon{$pos_codon}{"pos"}."\n";
# 					<STDIN>;
					$store{$sample}{$pos_codon}{"type"}="VR_A";
					if ($tab[35]>=0.25){
						$count_summary{"VR_A"}{"high_entropy"}++;
					}
					else{
						$count_summary{"VR_A"}{"low_entropy"}++;
					}
				}
			}
			else{
				if ($tab[35]>=0.25){
					$count_summary{"non_VR_A"}{"high_entropy"}++;
# 					print "\t\t$pos_codon\t$tab[35]\n";
# 					<STDIN>;
				}
				else{
					$count_summary{"non_VR_A"}{"low_entropy"}++;
				}
			}
		}
	}
	close $tsv;
# 	<STDIN>;
	### Backfilling the residues with 0 entropy just in case
	foreach my $codon (sort keys %check_codon){
		foreach my $sample (sort keys %check_sample){
			if (!defined($store{$sample}{$codon})){
				print "## $codon not reported for $sample in Anvi'o, we asigned it an entropy of 0\n";
				if ($check_codon{$codon}{"pos"}=~/;$/){chop($check_codon{$codon}{"pos"});}
				$store{$sample}{$codon}{"position"}=$check_codon{$codon}{"pos"};
				$store{$sample}{$codon}{"coverage"}="NA";
				$store{$sample}{$codon}{"reference"}="NA";
				$store{$sample}{$codon}{"consensus"}="NA";
				$store{$sample}{$codon}{"entropy"}=0;
				if ($check_codon{$codon}{"type"} eq "Random_nonVR_A"){
					$store{$sample}{$codon}{"type"}="Background";
				}
				else{
					$store{$sample}{$codon}{"type"}="VR_A";
				}
			}
		}
	}

	print "## Type,Entropy>=0.25,Entropy<0.25\n";
	print "VR_A,".$count_summary{"VR_A"}{"high_entropy"}.",".$count_summary{"VR_A"}{"low_entropy"}."\n";
	print "non_VR_A,".$count_summary{"non_VR_A"}{"high_entropy"}.",".$count_summary{"non_VR_A"}{"low_entropy"}."\n";
	return(%store);
}

sub test_filtered{
	my $bam_file=$_[0];
	open my $bam,"samtools view $bam_file |";
	my $n=0;
	while(<$bam>){
		chomp($_);
		$n++;
	}
	close $bam;
	return($n);
}

sub get_aa_decode {
    my $in_file="Table_decode_aa.tsv";
    my %tmp;
    open my $tsv,"<",$in_file;
    while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	$tmp{"1L"}{$tab[0]}{"full"}=$tab[1];
	$tmp{"1L"}{$tab[0]}{"3L"}=$tab[2];

	$tmp{"full"}{$tab[1]}{"1L"}=$tab[0];
	$tmp{"full"}{$tab[1]}{"3L"}=$tab[2];

	$tmp{"3L"}{$tab[2]}{"1L"}=$tab[0];
	$tmp{"3L"}{$tab[2]}{"full"}=$tab[1];
    }
    close $tsv;
    return %tmp;
}

sub test_avail{
	my $to_test=$_[0];
	my $path=&run_cmd("which ".$to_test);
	if ($path eq ""){die("pblm, we did not find the $to_test path, was Anvi'o correctly installed and/or the right environment loaded ?\n");}
}

sub run_cmd{
	my $cmd=$_[0];
	my $out=`$cmd`;
	if ($_[1] ne "quiet" && $_[1] ne "veryquiet"){
		if ($_[1] eq "stderr"){print STDERR "$out\n";}
		else{print "$out\n";}
	}
	return($out);
}


sub generate_anvio_files{
	my $gff_file=$_[0];
	my $id=$_[1];
	my $dir=$_[2];

	my $gene_calls_file=$dir."/".$id."_selected_gene_calls.txt";
	if ($gff_file eq $gene_calls_file){die("Pblm trying to come up with gene calls file from $gff_file\n");}

	my $gene_annot_file=$gff_file;
	my $gene_annot_file=$dir."/".$id."_selected_gene_annot.txt";
	if ($gff_file eq $gene_annot_file){die("Pblm trying to come up with gene annot file from $gff_file\n");}

	my $translate_id=$dir."/".$id."_selected_translate_gene_anvio.txt";
	if ($gff_file eq $translate_id){die("Pblm trying to come up with gene translate file from $gff_file\n");}

	my $seq_len=0;

	my $n=0;
	open my $s1,">",$gene_calls_file;
	print $s1 "gene_callers_id\tcontig\tstart\tstop\tdirection\tpartial\tsource\tversion\n";
	open my $s2,">",$gene_annot_file;
	print $s2 "gene_callers_id\tsource\taccession\tfunction\te_value\n";
	open my $s3,">",$translate_id;
	print $s3 "# Gene id\tId in anvi'o\n";
	open my $gff,"<",$gff_file;
	while(<$gff>){
	        chomp($_);
	        if ($_=~/^#/){
	                if ($_=~/sequence-region/){
	                        my @tab=split(" ",$_);
	                        $seq_len=$tab[3];
	                }
	                next;
	        }
	        my @tab=split("\t",$_);
	        if ($tab[2] eq "CDS"){
	                $n++;
	                my $strand="f";
	                if ($tab[6] eq "-"){$strand="r";}
	                $tab[3]--; ## Anvio count with a different starting point than prodigal / other gene callers
	                my $type="0"; ## 0 = complete, 1 = partial ### Set everyting as complete otherwise it's not included in the SNPs / SAVs calculations
	                if (($tab[4]-$tab[3]) % 3 ==0){}
	                else{
	                        print "Pblm with $tab[0] - $tab[3] - $tab[4] - $strand ??\n";
	                        die("\n");
	                }
	                print $s1 $n."\t".$tab[0]."\t".$tab[3]."\t".$tab[4]."\t".$strand."\t".$type."\tIMG\tv4\n";
	                my $id="";
	                if ($tab[8]=~/ID=([^;]+)/){$id=$1;}
	                else{die("pblm, no id for gene $_ - $tab[8]\n");}
	                print $s3 $id."\t".$n."\n";
	                my $product_line="";
	                if ($tab[8]=~/product=([^;]+)/){$product_line=$1;}
	                if ($product_line eq ""){
	                        print $s2 $n."\tIMG\t\t\t\n";
	                }
	                else{
	                        my @t=split(" ",$product_line);
	                        ### NOTE THIS IS A FAKE E-VALUE, IT'S ACTUALLY 10^-SCORE, BECAUSE WE DIDN'T STORE THE EVALUE, AND IT SHOULDN'T MATTER
	                        my $evalue=10**(-1*$t[2]);
	                        print $s2 $n."\tIMG\t".$t[0]."\t".$t[1]."\t".$evalue."\n";
	                }
	        }
	}
	close $gff;
	close $s1;
	close $s2;
	close $s3;
	#  The file must have these columns: 'gene_callers_id' (a
	#                        unique integer number for each gene call, start from
	#                        1), 'contig' (the contig name the gene call is found),
	#                       'start' (start position, integer), 'stop' (stop
	#                        position, integer), 'direction' (the direction of the
	#                        gene open reading frame; can be 'f' or 'r'), 'partial'
	#                        (whether it is a complete gene call, or a partial one;
	#                        must be 1 for partial calls, and 0 for complete
	#                        calls), 'source' (the gene caller), and 'version' (the
	#                        version of the gene caller, i.e., v2.6.7 or v1.0). An
	#                        example file can be found via the URL

}



sub revcomp(){
	my $nuc=$_[0];
	$nuc=~tr/atcguryswkmbdhvn/tagcayrswmkvhdbn/;
	$nuc=~tr/ATCGURYSWKMBDHVN/TAGCAYRSWMKVHDBN/;
	$nuc=reverse($nuc);
	return $nuc;
}

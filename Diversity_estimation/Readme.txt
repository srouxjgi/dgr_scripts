### How to run:
## Step 1
./Get_read_mapping.pl -d Example/ -i Meta_3300007360_Ga0104766_100029114 -r Example/SRR060353_subset_R1.fastq.gz-t 8 -p ~/path/to/FilterBam/filterBam/bin/filterBam
## Note: ~/path/to/FilterBam/filterBam/bin/filterBam must be replaced by the path to filterBam on your system
## Step 2
./Identify_SNVs.pl -d Example/ -i Meta_3300007360_Ga0104766_100029114 -r Example/SRR060353_subset_R1.fastq.gz -f ~/path/to/freebayes-v1.3.1
## Note: ~/path/to/freebayes-v1.3.1 must be replaced by the path to freebayes on your system
## Step 3
./Get_SAV_Anvio.pl -d Example/ -i Meta_3300007360_Ga0104766_100029114 -r Example/SRR060353_subset_R1.fastq.gz

### Requirements:
bwa
bbtools
samtools
bcftools
filterBam (https://github.com/nextgenusfs/augustus/tree/master/auxprogs/filterBam)
BioPerl Bio::Seq, Bio::SeqIO, Bio::SeqUtils, and Bio::Tools::CodonTable;
Muscle v3.8 (for realignment of TR-VR in Step 2)
Anvi'o v6.1+ (for step 3)


### Input files (example):
Nucleotide fasta file of the region of interest (e.g. Meta_3300007360_Ga0104766_100029114_selected.fna)
Gff file of the region of interest (e.g. Meta_3300007360_Ga0104766_100029114_selected.gff)
Information file about DGR locus (e.g. Meta_3300007360_Ga0104766_100029114_info.tsv)



### Notes:
# Example of command line used to download reads from SRA
fastq-dump --clip --skip-technical --read-filter pass --dumpbase --split-3 --gzip

#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Bio::Seq;
use Bio::SeqIO;
use Bio::Tools::CodonTable;
my $h='';
my $cmd='';
my $out='';
my $wdir='';
my $dgr_code='';
my $merged_reads_1='';
my $path_freebayes='';
GetOptions ('help' => \$h, 'h' => \$h, 'd=s'=>\$wdir, 'i=s'=>\$dgr_code, 'r=s'=>\$merged_reads_1, 'f=s'=>\$path_freebayes);
if ($h==1 || $wdir eq "" || $dgr_code eq "" || $merged_reads_1 eq ''){ # If asked for help or did not set up any argument
	print "# Script to identify SNVs around a DGR locus
# Arguments :
# -d: working directory
# -i: DGR code
# -r: read file R1 (fastq.gz)
# -f: path_freebayes
";
	die "\n";
}
if (!($wdir=~/\/$/)){$wdir.="/";}

## Setup everything
my $info_file=$wdir.$dgr_code."_info.tsv";
my $bam_dir=$wdir.$dgr_code."/";
if (!(-d $bam_dir)){print "#$#$# pblm $bam_dir doesn't exist\n"; next;}
my $ref_fna=$wdir.$dgr_code."_selected.fna";
my $gff_file=$wdir.$dgr_code."_selected.gff";
if (!(-e $ref_fna)){die("#$#$# Pblm, we don't find $ref_fna\n");}
if (!(-e $gff_file)){die("#$#$# Pblm, we don't find $gff_file\n");}
print "Checking read mapping \n";
$merged_reads_1=~/.*?([^\/]+)_R1\.fastq.gz/;
my $taxon_id=$1;
my $bam_sorted=$bam_dir.$taxon_id."_cover50_globali_sorted.bam";
if (!(-e $bam_sorted)){die("## Sorted bam file doesn't exist ($bam_sorted), we stop there\n");}
my $test=&test_filtered($bam_sorted);
if ($test==0){die("## Sorted bam file is empty ($bam_sorted), we stop there\n");}
my $myCodonTable = Bio::Tools::CodonTable->new();
my $syn_sites=get_syn_sites();




print "Read information about DGR locus\n";
my %info_contig;
open my $tsv,"<",$info_file;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	$info_contig{$tab[0]}=$tab[1];
}
close $tsv;
my $l=$info_contig{"Selected_end"}-$info_contig{"Selected_start"};
print "Check that we have enough coverage depth\n";
my $cover_file=$bam_sorted.".cover";
if (!(-e $cover_file)){
	&run_cmd("samtools depth -a -d 999999999 $bam_sorted > $cover_file","stderr");
}
open my $tsv,"<",$cover_file;
my $n=0;
my $total=0;
while(<$tsv>){
	chomp($_);
	my @t=split("\t",$_);
	$total+=$t[2];
	$n++;
}
close $tsv;
my $ratio=$n/$l;
my $avg=$total/$l;
my $log_file=$bam_dir."/log_mpileup.txt";
my $log_file_fb=$bam_dir."/log_freebayes.txt";
if ($avg<2){die("Average coverage is $avg, we stop there\n");}


print "Call SNVs with mpileup if not already done\n";
my $snp_file=$bam_sorted.".mpileup.bcf.SNP.txt";
my $snp_file_tr=$bam_sorted.".mpileup.bcf.SNP.translated.txt";
my $result_mpileup=$bam_sorted.".mpileup";
my $pnps_file=$bam_sorted.".mpileup.pnps";
if (!(-e $snp_file)){&mpileup_process($bam_sorted,$ref_fna);}
my %store_SNPs=%{&load_all_SNPs($snp_file)};
### Call Pn/PS if not already done
my %store_genes=%{&load_info_gff($gff_file,$ref_fna)};
print "Calculate pN/pS is not already done\n";
&calculate_pnps(\%store_SNPs,\%store_genes,$bam_sorted,$pnps_file);


print "Now calling SNVs with freebayes if not already done\n";
my $snp_file_fb=$bam_sorted.".freebayes.vcf.SNP.txt";
my $snp_file_fb_tr=$bam_sorted.".mpileup.bcf.SNP.translated.txt";
my $pnps_file_fb=$bam_sorted.".freebayes.pnps";
if (!(-e $path_freebayes)){
	print "Pblm with the path to the freebayes executable ($path_freebayes), skipping this step\n";
}
else{
	if (!(-e $snp_file_fb)){&freebayes_process($bam_sorted,$ref_fna);}
	my %store_SNPs=%{&load_all_SNPs($snp_file_fb)};
	print "Calculate pN/pS is not already done\n";
	&calculate_pnps(\%store_SNPs,\%store_genes,$bam_sorted,$pnps_file_fb);
}



print "Now linking SNVs to (predicted) DGR target positions\n";
my @tab_snp_method=("mp","fb");
my $final_file=$bam_dir."Parsed_results.csv";
my %check_gene_ok=();
open my $tsv,"<",$gff_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	$tab[8]=~/ID=([^;]+)/;
	my $gene_id=$1;
	if ($tab[2] eq "CDS"){
		$check_gene_ok{$gene_id}=1;
	}
}
close $tsv;
### Take info on contig
my $tag=-1;
my %info_contig=();
my %check_target=();
open my $tsv,"<",$info_file;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($tab[1] eq ""){next;}
	if ($tab[0]=~/^Target/){
		$tag=0;
		if ($check_gene_ok{$tab[1]}==1){
			$check_target{$tab[1]}=1;
			$tag=1;
		}
		else{print $tab[1]." is a target but not in the fragment, so we skip it\n";}
	}
	if ($tag==1){
		if ($tab[0]=~/(.*)_(\d+)/){
				$info_contig{"TRVR"}{"Repeat_".$2}{$1}=$tab[1];
				print "TRVR Repeat_".$2."\t".$1."\t".$tab[1]."\n";
		}
		else{$info_contig{$tab[0]}=$tab[1];}
	}
	elsif($tag==-1){$info_contig{$tab[0]}=$tab[1];}

}
close $tsv;
open my $fna,"<",$ref_fna;
while(<$fna>){
	chomp($_);
	if ($_=~/^>/){next;}
	$info_contig{"seq"}.=$_;
}
close $fna;
my @test=sort keys %check_target;
if ($#test<0){
	print "No target in the fragment, we stop there\n";
	die("\n");
}
my $total_length=$info_contig{"Selected_end"}-$info_contig{"Selected_start"}+1;
print "Checking that we have enough coverage and not too sparse\n";
my ($avg_cover,$ratio_cover)=&check_coverage($cover_file,$total_length);
if ($avg_cover<10){die("Only $avg_cover x average cover, we stop there\n");}
if ($ratio_cover < 0.8){die("Only ".sprintf("%.2f",$ratio_cover*100)." % positions covered we stop there\n");}

my $rest_l=$total_length;
my $gene_l=0;
my %pos_to_gene=();
open my $tsv,"<",$gff_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	$tab[8]=~/ID=([^;]+)/;
	my $gene_id=$1;
	if ($tab[2] eq "CDS"){
		$info_contig{"genes"}{$gene_id}{"start"}=$tab[3];
		$info_contig{"genes"}{$gene_id}{"stop"}=$tab[4];
		$info_contig{"genes"}{$gene_id}{"length"}=$tab[4]-$tab[3]+1;
		for (my $i=$tab[3];$i<=$tab[4];$i++){
			$pos_to_gene{$i}{$gene_id}=1;
		}
		$rest_l-=$info_contig{"genes"}{$gene_id}{"length"};
		$gene_l+=$info_contig{"genes"}{$gene_id}{"length"};
		$info_contig{"genes"}{$gene_id}{"strand"}=$tab[6];
	}
}
close $tsv;

### Calculate A positions in TR/VR
print "## Calculate which positions are interesting\n";
my %match_coords=();
my %check_edge=();
my $tr_l=0;
foreach my $repeat(sort keys %{$info_contig{"TRVR"}}){
	print "Looking at repeat $repeat\n";
	&get_info_repeat(\%info_contig,$repeat);
}

### SNPs mpileup
print "## Reading data from mpileup - $snp_file_tr / $pnps_file\n";
my %store=();
my %store_SNP=();
my %store_gene=();
open my $tsv,"<",$snp_file_tr;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	$store_SNP{"mp"}{$tab[1]}{"variants"}=$tab[2];
	$store_SNP{"mp"}{$tab[1]}{"ancestral"}=$tab[3];
	$store_SNP{"mp"}{$tab[1]}{"translate"}=$tab[4];
	if ($tab[4]=~/\S+,([^-]+)-[^-]+\s/){
		$store_SNP{"mp"}{$tab[1]}{"codon"}=$1;
		print "From mp -> $tab[1] corresponds to codon $1\n";
	}
	else{
		print "From mp -> I don't understand $tab[4]\n";
	}
}
close $tsv;
foreach my $snp (sort {$a <=> $b} keys %{$store_SNP{"mp"}}){
	my @t=split(" ",$store_SNP{"mp"}{$snp}{"translate"});
	my @t2=split("-",$t[1]);
	my $type_snp="unk";
	if ($t2[0] eq ""){$type_snp="intergenic";}
	elsif ($t2[0] eq $t2[1]){$type_snp="syn";}
	else{$type_snp="nsyn";}
	print "\t -- $snp -- ".$store_SNP{"mp"}{$snp}{"variants"}." -- ".$store_SNP{"mp"}{$snp}{"ancestral"}." -- $t2[0] == $t2[1]\n";
	my $line_tmp="\t -- $snp -- ".$store_SNP{"mp"}{$snp}{"variants"}." -- ".$store_SNP{"mp"}{$snp}{"ancestral"}." -- $t2[0] == $t2[1]\n";
	if ($info_contig{"check_TR"}{$snp}==1){
		print $line_tmp;
		print "\t this is in the TR\n";
		$store{"mp"}{"SNP_in_TR"}{$type_snp}++;
		$store{"mp"}{"SNP_in_TR"}{"total"}++;
# 				$store_gene{"TR"}{"mp"}{$type_snp}++;
# 				<STDIN>;
	}
	elsif ($info_contig{"check_VR"}{$snp}==1){
		print $line_tmp;
		print "\t this is in the VR\n"; ## We store in store_gene because we don't have it in the other file
		my $code_vr=$info_contig{"type_VR"}{$snp};
		if ($info_contig{"check_A_VR"}{$snp}==1){
			print "\t\t and this corresponds to an A, we like it ! \n";
			$store{"mp"}{"SNP_in_VR_A"}{$code_vr}{$type_snp}++;
			$store{"mp"}{"SNP_in_VR_A"}{$code_vr}{"total"}++;
			$store_gene{$code_vr}{"mp"}{$type_snp}++;
			print "We add $type_snp to $code_vr\n";
			if($match_coords{"A_to_codon"}{$snp}{$info_contig{"TRVR"}{$code_vr}{"Target_gene"}} eq $store_SNP{"mp"}{$snp}{"codon"}){
				print $match_coords{"A_to_codon"}{$snp}{$info_contig{"TRVR"}{$code_vr}{"Target_gene"}}." is the same codon as ".$store_SNP{"mp"}{$snp}{"codon"}." -- all good\n";
			}
			else{
				print "!!!! PBLM -> ".$match_coords{"A_to_codon"}{$snp}{$info_contig{"TRVR"}{$code_vr}{"Target_gene"}}." vs ".$store_SNP{"mp"}{$snp}{"codon"}." -- NOT GOOD for $snp / $code_vr\n";
				print $snp."\t".$check_edge{$snp}."\n";
				if ($match_coords{"A_to_codon"}{$snp}{$info_contig{"TRVR"}{$code_vr}{"Target_gene"}} eq "" && $check_edge{$snp}==1){
					print "Actually, that's probably just because of a we are at the very end of the VR, so we allow this\n";
				}
				else{
					die("\n");
					# next;
				}
			}
			# <STDIN>;
		}
		else{
			$store{"mp"}{"SNP_in_VR_nonA"}{$code_vr}{$type_snp}++;
			$store{"mp"}{"SNP_in_VR_nonA"}{$code_vr}{"total"}++;
			$store_gene{$code_vr}{"mp"}{$type_snp}++;
		}
		# <STDIN>;
	}
	elsif ($type_snp eq "intergenic"){
		print "\t this is not in the TR or in the VR and is intergenic\n";
		$store{"mp"}{"SNP_intergenic"}{$type_snp}++;
		$store{"mp"}{"SNP_intergenic"}{"total"}++;
	}
	else{
		print "\t this is not in the TR or in the VR\n";
		$store{"mp"}{"SNP_other"}{$type_snp}++;
		$store{"mp"}{"SNP_other"}{"total"}++;
	}
}
### pn/ps
open my $tsv,"<",$pnps_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	my $type="not_target";
	if ($tab[0] eq $info_contig{"Target_gene"}){$type="target";}
	$store{"mp"}{"pnps"}{$type}{$tab[0]}=$tab[4];
	$store_gene{$tab[0]}{"mp"}{"pnps"}=$tab[4];
	$store_gene{$tab[0]}{"mp"}{"syn"}=$tab[5];
	$store_gene{$tab[0]}{"mp"}{"nsyn"}=$tab[6];
}
close $tsv;

### same with fb
print "## Reading data from freebayes - $snp_file_fb_tr / $pnps_file_fb\n";
if (-e $snp_file_fb_tr && -e $pnps_file_fb){
	open my $tsv,"<",$snp_file_fb_tr;
	while(<$tsv>){
		chomp($_);
		if ($_=~/^#/){next;}
		my @tab=split("\t",$_);
		$store_SNP{"fb"}{$tab[1]}{"variants"}=$tab[2];
		$store_SNP{"fb"}{$tab[1]}{"ancestral"}=$tab[3];
		$store_SNP{"fb"}{$tab[1]}{"translate"}=$tab[4];
	}
	close $tsv;
	foreach my $snp (sort {$a <=> $b} keys %{$store_SNP{"fb"}}){
		my @t=split(" ",$store_SNP{"fb"}{$snp}{"translate"});
		my @t2=split("-",$t[1]);
		my $type_snp="unk";
		if ($t2[0] eq ""){$type_snp="intergenic";}
		elsif ($t2[0] eq $t2[1]){$type_snp="syn";}
		else{$type_snp="nsyn";}
		print "\t -- $snp -- ".$store_SNP{"fb"}{$snp}{"variants"}." -- ".$store_SNP{"fb"}{$snp}{"ancestral"}." -- $t2[0] == $t2[1]\n";
		if ($info_contig{"check_TR"}{$snp}==1){
			print "\t this is in the TR\n";
			$store{"fb"}{"SNP_in_TR"}{$type_snp}++;
			$store{"fb"}{"SNP_in_TR"}{"total"}++;
	# 				$store_gene{"TR"}{"fb"}{$type_snp}++;
	# 				<STDIN>;
		}
		elsif ($info_contig{"check_VR"}{$snp}==1){
			print "\t this is in the VR\n";
			my $code_vr=$info_contig{"type_VR"}{$snp};
			if ($info_contig{"check_A_VR"}{$snp}==1){
				print "\t\t and this corresponds to an A, we like it ! \n";
				$store{"fb"}{"SNP_in_VR_A"}{$code_vr}{$type_snp}++;
				$store{"fb"}{"SNP_in_VR_A"}{$code_vr}{"total"}++;
				$store_gene{$code_vr}{"fb"}{$type_snp}++;
				print "We add $type_snp to $code_vr\n";
			}
			else{
				$store{"fb"}{"SNP_in_VR_nonA"}{$code_vr}{$type_snp}++;
				$store{"fb"}{"SNP_in_VR_nonA"}{$code_vr}{"total"}++;
				$store_gene{$code_vr}{"fb"}{$type_snp}++;
			}
			# <STDIN>;
		}
		elsif ($type_snp eq "intergenic"){
			print "\t this is not in the TR or in the VR and is intergenic\n";
			$store{"fb"}{"SNP_intergenic"}{$type_snp}++;
			$store{"fb"}{"SNP_intergenic"}{"total"}++;
		}
		else{
			print "\t this is not in the TR or in the VR\n";
			$store{"fb"}{"SNP_other"}{$type_snp}++;
			$store{"fb"}{"SNP_other"}{"total"}++;
		}
	}
	### pn/ps
	open my $tsv,"<",$pnps_file_fb;
	while(<$tsv>){
		chomp($_);
		if ($_=~/^#/){next;}
		my @tab=split("\t",$_);
		my $type="not_target";
		if ($tab[0] eq $info_contig{"Target_gene"}){$type="target";}
		$store{"pnps"}{$type}{$tab[0]}=$tab[4];
		$store_gene{$tab[0]}{"fb"}{"pnps"}=$tab[4];
		$store_gene{$tab[0]}{"fb"}{"syn"}=$tab[5];
		$store_gene{$tab[0]}{"fb"}{"nsyn"}=$tab[6];
	}
	close $tsv;
}
else{
	print "Freebayes not run, skipping\n";
}

print "Get coverage per gene\n";
my @tab_cover=();
my %store_cover=();
open my $tsv,"<",$cover_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	$store_cover{$tab[1]}=$tab[2];
	push(@tab_cover,$tab[2]);
	foreach my $gene (sort keys %{$pos_to_gene{$tab[1]}}){
# 				print $tab[1]." is in gene ".$gene."\n";
		if (defined($store_gene{$gene}{"cover"})){
			push(@{$store_gene{$gene}{"cover"}},$tab[2]);
		}
		else{
			my @t=($tab[2]);
			$store_gene{$gene}{"cover"}=\@t;
		}
	}
	if (defined($info_contig{"type_VR"}{$tab[1]})){
		my $gene=$info_contig{"type_VR"}{$tab[1]};
		if (defined($store_gene{$gene}{"cover"})){
			push(@{$store_gene{$gene}{"cover"}},$tab[2]);
		}
		else{
			my @t=($tab[2]);
			$store_gene{$gene}{"cover"}=\@t;
		}
	}
}
close $tsv;
$store{"median_cover"}=&median(\@tab_cover);
foreach my $gene (sort keys %store_gene){
	if (!defined($store_gene{$gene}{"cover"})){next;}
	$store_gene{$gene}{"median_coverage"}=&median($store_gene{$gene}{"cover"});
}
## And the pnps for the VR
foreach my $repeat (sort keys %{$info_contig{"TRVR"}}){
	foreach my $method (@tab_snp_method){
		if ($store_gene{$repeat}{$method}{"syn"}==0){
			$store_gene{$repeat}{$method}{"pnps"}="NA_No-syn";
		}
		else{
			my $e_ratio=$info_contig{"VR_info"}{$repeat}{"expected_ratio"};
			print "\t\tE ratio -> VR info / $method / $repeat / ".$info_contig{"VR_info"}{$repeat}{"expected_ratio"}."\n";
			my $o_ratio=$store_gene{$repeat}{$method}{"nsyn"}/$store_gene{$repeat}{$method}{"syn"};
			print "\t\tO ratio -> $method / $repeat / $o_ratio\n"; # <STDIN>;
			my $pnps=$o_ratio/$e_ratio;
			$store_gene{$repeat}{$method}{"pnps"}=$pnps;
			<STDIN>;
		}
	}
	#### Also calculating the associated stdev coverage on similar length sliding windows on non target genes
	my ($median_nontarget,$stdev_nontarget)=&calculated_stdev(\%store_cover,$info_contig{"genes"},\%check_target,length($info_contig{"TRVR"}{$repeat}{"VR_seq"}));
	print $repeat."\t".length($info_contig{"TRVR"}{$repeat}{"VR_seq"})."\t".$median_nontarget."\t".$stdev_nontarget."\n";
# 		<STDIN>;
	$store_gene{$repeat}{"median_random_nontarget"}=$median_nontarget;
	$store_gene{$repeat}{"stdev_random_nontarget"}=$stdev_nontarget;
}


open my $s2,">",$final_file;
## MP: mpileup
## FB: freebayes
print $s2 "## Metagenome\tDGR\tGene/Feature\tLength\tGene/Feature type\tMedian coverage\tMP_Synonymous\tMP_NonSynonymous\tMP_PnPS\tFB_Synonymous\tFB_NonSynonymous\tFB_PnPS\n";
foreach my $gene (sort keys %store_gene){
	if (defined($info_contig{"genes"}{$gene})){
		my $target="NonTarget";
		my $target_type="NonTarget";
		my $target_pc="NonTarget";
		if ($check_target{$gene}==1){
			$target="Target";
		}
		## Adjust values if needed for NA and 0s in pNpS calculations
		foreach my $method (@tab_snp_method){
			if ($store_gene{$gene}{$method}{"pnps"}=~/^NA/){
				$store_gene{$gene}{$method}{"pnps"}="NA";
			}
		}

		if (!defined($store_gene{$gene}{"mp"}{"syn"})){
			print "!!! $gene was not found at all ????\n";
			<STDIN>;
		}
		print $s2 $taxon_id."\t".$dgr_code."\t".$gene."\t".$info_contig{"genes"}{$gene}{"length"}."\t".$target."\t".$store_gene{$gene}{"median_coverage"}."\t".
		$store_gene{$gene}{"mp"}{"syn"}."\t".$store_gene{$gene}{"mp"}{"nsyn"}."\t".$store_gene{$gene}{"mp"}{"pnps"}."\t".$store_gene{$gene}{"fb"}{"syn"}."\t".
		$store_gene{$gene}{"fb"}{"nsyn"}."\t".$store_gene{$gene}{"fb"}{"pnps"}."\n";
	}
	else{
		my $target="VR";
		my $target_type="To_remove";
		my $target_pc="Unknown";
		my $target_gene=$info_contig{"TRVR"}{$gene}{"Target_gene"};
		my $vr_seq=$info_contig{"TRVR"}{$gene}{"VR_seq"};
		## Adjust values if needed for NA and 0s in pNpS calculations
		foreach my $method (@tab_snp_method){
			if (!defined($store_gene{$gene}{$method}{"syn"})){$store_gene{$gene}{$method}{"syn"}=0;}
			if (!defined($store_gene{$gene}{$method}{"nsyn"})){$store_gene{$gene}{$method}{"nsyn"}=0;}
			if ($store_gene{$gene}{$method}{"pnps"}=~/^NA/){
				$store_gene{$gene}{$method}{"pnps"}="NA";
			}
		}
		print $s2 $taxon_id."\t".$dgr_code."\t".$gene."\t".length($vr_seq)."\t".$target."\t".$store_gene{$gene}{"median_coverage"}."\t".
		$store_gene{$gene}{"mp"}{"syn"}."\t".$store_gene{$gene}{"mp"}{"nsyn"}."\t".$store_gene{$gene}{"mp"}{"pnps"}."\t".$store_gene{$gene}{"fb"}{"syn"}."\t".
		$store_gene{$gene}{"fb"}{"nsyn"}."\t".$store_gene{$gene}{"fb"}{"pnps"}."\n";
	}
}
close $s2;




## Functions
sub mpileup_process{
	my $bam_sorted=$_[0];
	my $fasta=$_[1];
	my $result_mpileup=$bam_sorted.".mpileup";
	if (-e $result_mpileup){print "$result_mpileup already here\n";}
	else{
		&run_cmd("bcftools mpileup -A -Q 15 -L 8000 -d 8000 -f $fasta $bam_sorted > $result_mpileup","stderr");
	}
	# Then bcftools
	my $result_bcf=$result_mpileup.".bcf";
	if (-e $result_bcf){print "$result_bcf already here\n";}
	else{
		&run_cmd("bcftools call $result_mpileup --ploidy 1 -A -m -O b -o $result_bcf","stderr");
	}
	my $final_mpileup=$result_bcf.".SNP.txt";
	if (-e $final_mpileup){print "$final_mpileup already here\n";}
	else{
		my $c_c="";
		my $c_pos=0;
		my $cov=0;
		open my $tsv,"bcftools view $result_bcf |";
		open my $s1,">",$final_mpileup;
		while(<$tsv>){
			chomp($_);
			if ($_=~/^#/){next;}
			my @tab=split("\t",$_);
			my $pos=$tab[1];
			if($tab[7]=~/^INDEL/){
	# 			print "no SNP at this position $pos but INDEL, we don't care\n";
			}
			else{
				if ($tab[7]=~/^DP=(\d+)/){$cov+=$1;}
				else{print STDERR "!! Pblm in bcftools with line $_\n";} #<STDIN>;}
				if ($tab[4] eq "."){
	# 				print "no SNP at this position $pos\n";
				}
				else{
					my $anc=$tab[3];
					my @tab_nuc=split(",",$tab[4]);
					$tab[7]=~/DP4=([\d,]+);/;
					my @tab_depth=split(",",$1);
					my $line=$tab[0]."\t".$tab[1]."\t";
					my %store_depth;
					my $total=0;
					for(my $i=0;$i<=$#tab_depth;$i++){
						my $nuc=int($i / 2);
						if ($nuc<1){
							print STDERR "$tab_depth[$i] is $anc\n";
							$store_depth{$anc}+=$tab_depth[$i];
							$total+=$tab_depth[$i];
						}
						else{
							print STDERR "$tab_depth[$i] ->  added to nuc $nuc -> $tab_nuc[$nuc-1]\n";
							$store_depth{$tab_nuc[$nuc-1]}+=$tab_depth[$i];
							$total+=$tab_depth[$i];
						}
					}
					$line.=$anc."/".$store_depth{$anc}.",";
					my $n=0;
					foreach my $snp (@tab_nuc){
						if ($store_depth{$snp}<4 || $store_depth{$snp}/$total<0.01){ # Th from Schloissnig: 4 reads and >1%
							print STDERR "snp $snp has less than 4 reads or 1%, we don't consider - ".$store_depth{$snp}." - ".$total."\n";
							next;
						}
						$n++;
						$line.=$snp."/".$store_depth{$snp}.",";
						print STDERR "snp $snp has more than 4 reads, we take it\n";

					}
					if ($n>0){
						chop($line);
						$line.="\t".$anc;
						print $s1 "$line\n";
					}
		# 			<STDIN>;
				}
			}
		}
		close $tsv;
		close $s1;
	}
}

sub freebayes_process{
	my $bam_sorted=$_[0];
	my $fasta=$_[1];
	my $result_freebayes=$bam_sorted.".freebayes.vcf";
	if (-e $result_freebayes){print "$result_freebayes already here\n";}
	else{
		&run_cmd("$path_freebayes --ploidy 1 --min-base-quality 15 --haplotype-length 0 --min-alternate-count 1 --min-alternate-fraction 0 --pooled-continuous --limit-coverage 8000 -f $fasta $bam_sorted -v $result_freebayes","stderr");
	}
	my $final_freebayes=$result_freebayes.".SNP.txt";
	if (-e $final_freebayes){print "$final_freebayes already here\n";}
	else{
		my $c_c="";
		my $c_pos=0;
		my %store_line;
		my %store_depth;
		open my $tsv,"<",$result_freebayes;
		open my $s1,">",$final_freebayes;
		while(<$tsv>){
			chomp($_);
			if ($_=~/^#/){next;}
			my @tab_line=split("\t",$_);
			my $ori_pos=$tab_line[1];
			my $anc=$tab_line[3];
			%store_line=();
			if ($anc eq "N"){print STDERR "The ancestral/consensus is N, so not well assembled, we skip\n"; next;}
			if ($tab_line[4] eq "."){print "no SNP at this position $ori_pos\n"; next; }
			my $all_type="unknown";
			if ($tab_line[7]=~/TYPE=([^;]+)/){$all_type=$1;}
			if ($all_type eq "unknown"){print STDERR "!! Pblm in reading result from freebayes for type with line $_\n"; die("\n");}
			my @tab_type=split(",",$all_type);
			my @tab_new=split(",",$tab_line[4]);
			my @tab_header=split(":",$tab_line[8]);
			my @tab_info=split(":",$tab_line[9]);
			my $total;
			%store_depth=();
			for (my $j=0;$j<=$#tab_header;$j++){
				if ($tab_header[$j] eq "AD"){
					my @t=split(",",$tab_info[$j]);
# 					print "Anc $anc -> $t[0]\n";
					$store_depth{"anc"}=$t[0];
					$total+=$t[0];
					for (my $i=1;$i<=$#t;$i++){
						my $num=$i-1;
						$store_depth{"alt"}{$num}=$t[$i];
# 						print "Alt $tab_new[$num] - $num -> $t[$i]\n";
						$total+=$t[$i];
					}
				}
			}
			for (my $k=0;$k<=$#tab_type;$k++){
				my $type=$tab_type[$k];
				if($type eq "del"){print STDERR "Deletion, we skip\n"; next;}
				elsif($type eq "ins"){print STDERR "Insertion, we skip\n"; next;}
				elsif($type eq "mnp"){} ## Mnp are contiguous SNPs, we keep
				elsif($type eq "snp"){}
				elsif($type eq "complex"){}
				else{
					print STDERR "PBLM I don't understand $type\n";
					print join("\t",@tab_line)."\n";
					die("\n");
					next;
				}
				if ($type eq "snp" || $type eq "complex" || $type eq "mnp"){
					print STDERR "We have a $type with $anc vs $tab_new[$k]\n";
					if (length($anc) != length($tab_new[$k])){
						if ($type eq "complex"){
							print "This is a complex that involved some indels, we skip\n";
							next;
						}
						die("THIS SHOULD NEVER HAPPEN -> THESE VARIANTS SHOULD HAVE THE SAME LENGTH BECAUSE THEY ARE OF TYPE $type\n");
					}
					my @tab_anc=split("",$anc);
					my @tab_nuc=split("",$tab_new[$k]);

					if ($store_depth{"alt"}{$k}<4 || $store_depth{"alt"}{$k}/$total<0.01){ # Th from Schloissnig: 4 reads and >1%
						print STDERR "snp $tab_new[$k] has less than 4 reads or 1%, we don't consider - ".$store_depth{"alt"}{$k}." - ".$total."\n";
						next;
					}

					for (my $i=0;$i<=$#tab_anc;$i++){
						my $pos=$ori_pos+$i;
						my $anc=$tab_anc[$i];
						my $alt=$tab_nuc[$i];

						if (!defined($store_line{$pos}{"contig"})){$store_line{$pos}{"contig"}=$tab_line[0];}
						if (!defined($store_line{$pos}{"position"})){$store_line{$pos}{"position"}=$pos;}
						if (!defined($store_line{$pos}{"anc"})){$store_line{$pos}{"anc"}=$anc;}
						if (!defined($store_line{$pos}{"nucl"})){$store_line{$pos}{"nucl"}=$anc."/".$store_depth{"anc"}.",";}

						$store_line{$pos}{"nucl"}.=$alt."/".$store_depth{"alt"}{$k}.",";
						print STDERR "snp $alt has more than 4 reads, we take it - $pos == $anc vs $alt\n";
					}
				}
			}
			foreach my $pos (sort {$a <=> $b} keys %store_line){
				if ($store_line{$pos}{"nucl"}=~/,/){
					chop($store_line{$pos}{"nucl"});
					print $s1 $store_line{$pos}{"contig"}."\t".$store_line{$pos}{"position"}."\t".$store_line{$pos}{"nucl"}."\t".$store_line{$pos}{"anc"}."\n";
				}
			}
		}
		close $tsv;
		close $s1;
	}
}


sub load_info_gff{
	my %temp;
	my $gff_file=$_[0];
	my $fna_file=$_[1];
	open my $fa,"<",$fna_file;
	while(<$fa>){
		chomp($_);
		if ($_=~/^>(.*)/){$temp{"accession"}=$1;$temp{"taxonomy"}="Environmental sample";}
		else{
			$_=~tr/atcgn/ATCGN/;
			$temp{"seq"}.=$_;
		}
	}
	close $fa;
	my $seq=$temp{"seq"};
	$temp{"length"}=length($temp{"seq"});
	open my $gff,"<",$gff_file;
	while(<$gff>){
		chomp($_);
		if ($_=~/^##sequence-region/){
			my @tab=split(" ",$_);
			$temp{"name"}=$tab[1];
			next;
		}
		elsif($_=~/^##species/){next;}
		my @tab=split("\t",$_);
		if ($tab[2] eq "region"){next;}
		if ($tab[2] eq "gene"){
			$temp{"feature"}{$tab[3]}{"gene"}{"start"}=$tab[3];
			$temp{"feature"}{$tab[3]}{"gene"}{"end"}=$tab[4];
			$temp{"feature"}{$tab[3]}{"gene"}{"strand"}=$tab[6];
			my %temp_hash=split(/[;=]/,$tab[8]);
			$temp{"feature"}{$tab[3]}{"gene"}{"gene"}=$temp_hash{"ID"};
			$temp{"feature"}{$tab[3]}{"gene"}{"note"}=$temp_hash{"note"};
		}

		elsif ($tab[2] eq "CDS"){
			$temp{"feature"}{$tab[3]}{"CDS"}{"start"}=$tab[3];
			$temp{"feature"}{$tab[3]}{"CDS"}{"end"}=$tab[4];
			$temp{"feature"}{$tab[3]}{"CDS"}{"strand"}=$tab[6];
			my %temp_hash=split(/[;=]/,$tab[8]);
			$temp{"feature"}{$tab[3]}{"CDS"}{"locus_tag"}=$temp_hash{"locus_tag"};
			$temp{"feature"}{$tab[3]}{"CDS"}{"note"}=$temp_hash{"note"};
			$temp{"feature"}{$tab[3]}{"CDS"}{"product"}=$temp_hash{"product"};
			# NOW TAKE THE INFORMATION NEEDED FOR THE PNPS
			my $start=$tab[3]-1;
			my $end=$tab[4];
			my $strand=$tab[6];
			my $n=0;
			my $len=$end-$start;
			my $gene_sqce;
			if($start<0 && $strand eq "+"){
				$start+=3;
				$len=$end-$start;
			}
			if ($len % 3 ==0){}
			else{print STDERR "Length $len - $start / $end ????\n";}
			if ($strand eq "+"){
				$gene_sqce=substr($seq,$start,$len);
			}
			elsif($strand eq "-"){
				$gene_sqce=substr($seq,$start,$len);
				$gene_sqce=reverse($gene_sqce);
				$gene_sqce=~tr/atcgATCG/tagcTAGC/;
			}
			else{die("!?!?!? What is $strand ? $_\n");}
			$temp{"feature"}{$tab[3]}{"CDS"}{"sequence"}=$gene_sqce;
			print STDERR "CDS $tab[3] - $start to $end - $gene_sqce\n";
			my @t=split("",$gene_sqce);
			my $total_syn=0;
			my $total_nsyn=0;
			for (my $i=0;$i<=$#t;$i+=3){
				my $cod=substr($gene_sqce,$i,3);
				print "$i - $cod\n";
				for (my $j=0;$j<=2;$j++){
					print STDERR "\t$j - $$syn_sites{$cod}{$j}{s} - $$syn_sites{$cod}{$j}{n}\n";
					$total_syn+=$$syn_sites{$cod}{$j}{"s"};
					$total_nsyn+=$$syn_sites{$cod}{$j}{"n"};
				}
			}
			my $expected_ratio=$total_nsyn/$total_syn;
			print STDERR "###\n";
			print STDERR "$temp{feature}{$tab[3]}{CDS}{locus_tag} - $temp{feature}{$tab[3]}{CDS}{function} - $total_syn - $total_nsyn - $expected_ratio\n";
			$temp{"feature"}{$tab[3]}{"CDS"}{"expected_nsyn"}=$total_nsyn;
			$temp{"feature"}{$tab[3]}{"CDS"}{"expected_syn"}=$total_syn;
		}
	}
	return \%temp;
}


sub load_all_SNPs{
	my $result_mpileup=$_[0];
	my %temp;
	open my $txt,"<",$result_mpileup;
	while(<$txt>){
		chomp($_);
		my @tab=split("\t",$_);
		my @t=split(",",$tab[2]);
		$temp{$tab[1]}{"anc"}=$tab[3];
		foreach my $n (@t){
			$n=~/([ATCG])\/(\d+)/;
			my $nuc=$1;
			my $depth=$2;
			if ($nuc eq $tab[3]){}
			else{
				$temp{$tab[1]}{$nuc}=$depth;
				print STDERR "Mpileup -SNP at $tab[1] - $nuc ".$depth."x\n";;
			}
		}
	}
	close $txt;
	return \%temp;
}


sub calculate_pnps{
	my %input=%{$_[0]};
	my %info=%{$_[1]};
	my $bam_file=$_[2];
	my $outfile_pnps=$_[3];
	my $snp_file="";
	my $snp_file_with_translation="";
	if ($outfile_pnps=~/mpileup/){
		$snp_file=$_[2].".mpileup.bcf.SNP.txt";
		$snp_file_with_translation=$_[2].".mpileup.bcf.SNP.translated.txt";
	}
	elsif($outfile_pnps=~/freebayes/){
		$snp_file=$_[2].".freebayes.vcf.SNP.txt";
		$snp_file_with_translation=$_[2].".freebayes.vcf.SNP.translated.txt";
	}
	my $anc="";
	my %count;
	my %store_pos;
	my $myCodonTable = Bio::Tools::CodonTable->new();
	print STDERR "listing synonymous vs non-synonymous codons/SNPs\n";
	foreach my $pos (sort {$a <=> $b} keys %input){
		foreach my $snp (keys %{$input{$pos}}){
			my $store_snp=$snp;
			if ($snp eq "anc"){next;}
			my $tag=0;
			foreach my $gene_start (sort {$a <=> $b} keys %{$info{"feature"}}){
				$anc=$input{$pos}{"anc"};
				if ($info{"feature"}{$gene_start}{"CDS"}{"start"}<=$pos && $info{"feature"}{$gene_start}{"CDS"}{"end"}>=$pos){
					$tag=1;
					my $locus_tag=$info{"feature"}{$gene_start}{"CDS"}{"locus_tag"};
					print STDERR "position $pos -> corresponds to gene $gene_start // $locus_tag -> ".$info{"feature"}{$gene_start}{"CDS"}{"strand"}."\n";
					my $pos_gene=-1;
					print STDERR $info{"feature"}{$gene_start}{"CDS"}{"sequence"}."\n";
					my $seqtmp= Bio::Seq->new( -display_id => 'my_id',-seq => $info{"feature"}{$gene_start}{"CDS"}{"sequence"});
					print STDERR $seqtmp->translate()->seq."\n";
					if($info{"feature"}{$gene_start}{"CDS"}{"strand"}==-1 || $info{"feature"}{$gene_start}{"CDS"}{"strand"} eq "-"){
						print STDERR "we are on the back strand\n";
						print STDERR "$pos - $info{feature}{$gene_start}{CDS}{start}\n";
						$snp=&revcomp($snp);
						$anc=&revcomp($anc);
						$pos_gene=$info{"feature"}{$gene_start}{"CDS"}{"end"}-$pos;
					}
					else{
						$pos_gene=$pos-$info{"feature"}{$gene_start}{"CDS"}{"start"};
						print STDERR "$pos - $info{feature}{$gene_start}{CDS}{start}\n";
					}
					print STDERR " - position in gene : $pos_gene\n";
					# store the current codon and position number
					my ($cod_number, $pos_in_codon) = (int $pos_gene / 3, $pos_gene % 3);
					my $cod=substr($info{"feature"}{$gene_start}{"CDS"}{"sequence"},$cod_number*3,3);
					if ($cod eq ""){
						print "!!! No codon for $cod_number - $gene_start ?\n";
						next;
					}
					my $aa=$myCodonTable->translate($cod);
					print STDERR " - position $pos_in_codon in codon $cod_number => $cod\n";
					my $current=substr($cod,$pos_in_codon,1);
					print STDERR "Current nucleotide is $current - check, should be $anc\n";
					if ($current ne $anc){
						print "!! I don't understand $current vs $anc!\n";
					}
					# calculate alternate codon
					my $alternate=$cod;
					substr($alternate,$pos_in_codon,1)=$snp;
					print STDERR " - SNP is $snp at pos $pos_in_codon -> alternate codon is $alternate\n";
					my $alternate_aa=$myCodonTable->translate($alternate);
					print STDERR " - $cod -> $aa vs $alternate -> $alternate_aa\n";
					$store_pos{$pos}{$store_snp}{$locus_tag}{"original_codon"}=$cod;
					$store_pos{$pos}{$store_snp}{$locus_tag}{"alternate_codon"}=$alternate;
					$store_pos{$pos}{$store_snp}{$locus_tag}{"original_aa"}=$aa;
					$store_pos{$pos}{$store_snp}{$locus_tag}{"alternate_aa"}=$alternate_aa;
					# add +1 to syn or nonsyn depending on the outcome
					$count{"pnps"}{$gene_start}{"total"}++;
					if ($alternate_aa eq $aa) {
						$count{"pnps"}{$gene_start}{"syn"}++;
						print STDERR " - - - so we add one to the synonymous mutations\n";
					}
					else{
						$count{"pnps"}{$gene_start}{"nsyn"}++;
						print STDERR " - - - so we add one to the non-synonymous mutations\n";
					}
				}
			}
			if ($tag==0){
				$count{"pnps"}{"IG"}{"total"}++;
			}
		}
	}
	print STDERR "now calculating the real pn/ps ratios\n";
	open my $s1,">",$outfile_pnps;
	print $s1 "## Locus tag\tProduct\tExpected ratio\tObserved ratio\tPnPs\tN syn\tN nonsyn\n";
	foreach my $gene_start (sort {$a <=> $b} keys %{$info{"feature"}}){
		my $pnps="NA";
		my $expected_ratio="NA";
		my $observed_ratio="NA";
		if ($info{"feature"}{$gene_start}{"CDS"}{"expected_syn"}==0){
			print STDERR $info{"feature"}{$gene_start}{"CDS"}{"locus_tag"}." - $gene_start - expected syn is 0 ??? - ".$info{"feature"}{$gene_start}{"CDS"}{"expected_nsyn"}." - ".$info{"feature"}{$gene_start}{"CDS"}{"expected_syn"}." - ".$count{"pnps"}{$gene_start}{"nsyn"}." - ".$count{"pnps"}{$gene_start}{"syn"}."\n";
			$pnps="NA_expectedsyn";
		}
		else{
			$expected_ratio=$info{"feature"}{$gene_start}{"CDS"}{"expected_nsyn"}/$info{"feature"}{$gene_start}{"CDS"}{"expected_syn"};
			if ($count{"pnps"}{$gene_start}{"syn"}==0){
				print STDERR $info{"feature"}{$gene_start}{"CDS"}{"locus_tag"}." - $gene_start - observed syn is 0 ??? - ".$info{"feature"}{$gene_start}{"CDS"}{"expected_nsyn"}." - ".$info{"feature"}{$gene_start}{"CDS"}{"expected_syn"}." - ".$count{"pnps"}{$gene_start}{"nsyn"}." - ".$count{"pnps"}{$gene_start}{"syn"}."\n";
				$pnps="NA_syn";
			}
			else{
				$observed_ratio=$count{"pnps"}{$gene_start}{"nsyn"}/$count{"pnps"}{$gene_start}{"syn"};
				if ($expected_ratio==0){
					print STDERR $info{"feature"}{$gene_start}{"CDS"}{"locus_tag"}." - $gene_start - expected is 0 ??? - ".$info{"feature"}{$gene_start}{"CDS"}{"expected_nsyn"}." - ".$info{"feature"}{$gene_start}{"CDS"}{"expected_syn"}." - ".$count{"pnps"}{$gene_start}{"nsyn"}." - ".$count{"pnps"}{$gene_start}{"syn"}."\n";
					$pnps="NA_expected";
				}
				else{
					$pnps=$observed_ratio/$expected_ratio;
				}
			}
		}
		print STDERR "$info{feature}{$gene_start}{CDS}{locus_tag}\t$info{feature}{$gene_start}{CDS}{product}\t$expected_ratio\t$observed_ratio\t$pnps\n";
		if (!defined($count{"pnps"}{$gene_start}{"syn"})){$count{"pnps"}{$gene_start}{"syn"}=0;}
		if (!defined($count{"pnps"}{$gene_start}{"nsyn"})){$count{"pnps"}{$gene_start}{"nsyn"}=0;}
		print $s1 "$info{feature}{$gene_start}{CDS}{locus_tag}\t$info{feature}{$gene_start}{CDS}{product}\t$expected_ratio\t$observed_ratio\t$pnps\t$count{pnps}{$gene_start}{syn}\t$count{pnps}{$gene_start}{nsyn}\n";
	}
	close $s1;
	print STDERR "now decorating the SNP file\n";
	open my $s1,">",$snp_file_with_translation;
	open my $tsv,"<",$snp_file;
	while(<$tsv>){
		chomp($_);
		my @tab=split("\t",$_);
		my @t=split(",",$tab[2]);
		my @t2;
		foreach my $n (@t){
			$n=~/([ATCG])\/\d+/;
			my $nuc=$1;
			if ($nuc eq $tab[3]){}
			else{
				foreach my $locus_tag (sort keys %{$store_pos{$tab[1]}{$nuc}}){
					print $s1 $_."\t".$locus_tag.",".$store_pos{$tab[1]}{$nuc}{$locus_tag}{"original_codon"}."-".$store_pos{$tab[1]}{$nuc}{$locus_tag}{"alternate_codon"}." ".$store_pos{$tab[1]}{$nuc}{$locus_tag}{"original_aa"}."-".$store_pos{$tab[1]}{$nuc}{$locus_tag}{"alternate_aa"}."\n";
				}
			}
		}
	}
	close $tsv;
	close $s1;
}


sub get_syn_sites(){
	my @nucs = qw(T C A G);
	my %raw_results;
	my $myCodonTable = Bio::Tools::CodonTable->new();
	for my $i (@nucs) {
		for my $j (@nucs) {
			for my $k (@nucs) {
			# for each possible codon
				my $cod = "$i$j$k";
				my $aa=$myCodonTable->translate($cod);
				#calculate number of synonymous mutations vs non syn mutations
				for my $i (qw(0 1 2)){
					my $s = 0;
					my $n = 0;
					for my $nuc (qw(A T C G)) {
						next if substr ($cod, $i,1) eq $nuc;
						my $test = $cod;
						substr($test, $i, 1) = $nuc ;
						if ($myCodonTable->translate($test) eq $aa) {
							$s++;
						}
						else{
							$n++;
						}
						if ($myCodonTable->translate($test) eq '*') {
							$n--;
						}
					}
					$raw_results{$cod}{$i} = {'s' => $s , 'n' => $n };
# 					$raw_results{$cod}{$i} = $s/($n+$s);
					if ($raw_results{$cod}{$i}==0.5){
						print STDERR "$cod $i\n";
					}
				}
			} #end analysis of single codon
		}
	} #end analysis of all codons
	return \%raw_results;
}

sub get_info_repeat{
	my $info_contig=$_[0];
	my $repeat=$_[1];
	my $tr_seq=$$info_contig{"TRVR"}{$repeat}{"TR_seq"};
	my $vr_seq=$$info_contig{"TRVR"}{$repeat}{"VR_seq"};
	my @tab_vr_coord=split(":",$$info_contig{"TRVR"}{$repeat}{"VR_shifted"});
	my @tab_tr_coord=split(":",$$info_contig{"TRVR"}{$repeat}{"TR_shifted"});
	print "$tr_seq\n";
	print "$vr_seq\n";
	print join(":",@tab_tr_coord)."\n";
	print join(":",@tab_vr_coord)."\n";
	## Trim VR / TR if beyond the gene
	my $start=$$info_contig{"genes"}{$$info_contig{"TRVR"}{$repeat}{"Target_gene"}}{"start"};
	my $end=$$info_contig{"genes"}{$$info_contig{"TRVR"}{$repeat}{"Target_gene"}}{"stop"};
	if ($start>$tab_vr_coord[0]){
		my $diff=$start-$tab_vr_coord[0];
		$tab_vr_coord[0]=$start;
		$tab_tr_coord[0]=$tab_tr_coord[0]+$diff;
		print "Gene start at $start, new tr/vr is:\n";
		print $tr_seq."\n";
		print $vr_seq."\n";
		print join(":",@tab_tr_coord)."\n";
		print join(":",@tab_vr_coord)."\n";
		# <STDIN>;
	}
	if ($end<$tab_vr_coord[1]){
		my $diff=$tab_vr_coord[1]-$end;
		$tab_vr_coord[1]=$end;
		$tab_tr_coord[1]=$tab_tr_coord[1]-$diff;
		print "Gene ends at $end, new tr/vr is:\n";
		print $tr_seq."\n";
		print $vr_seq."\n";
		print join(":",@tab_tr_coord)."\n";
		print join(":",@tab_vr_coord)."\n";
	}
	if (length($tr_seq) == length($vr_seq)){
		## All good, no gaps
	}
	else{
		print "Difference in length between TR and VR, so we re-align\n";
		print $tr_seq."\n";
		print $vr_seq."\n";
		($tr_seq,$vr_seq)=&realign($tr_seq,$vr_seq);
		print $tr_seq."\n";
		print $vr_seq."\n";
		# <STDIN>;
	}

	if ($tr_l==0){$tr_l=length($tr_seq);}
	## Calculate on which strand we are
	my $type="unknown";
	if ($$info_contig{"genes"}{$$info_contig{"TRVR"}{$repeat}{"Target_gene"}}{"strand"} eq "+"){
		$type="plus";
	}
	elsif ($$info_contig{"genes"}{$$info_contig{"TRVR"}{$repeat}{"Target_gene"}}{"strand"} eq "-"){
		$type="minus";
	}
	else{
		print "!!! Pblm - My target gene is ".$$info_contig{"TRVR"}{$repeat}{"Target_gene"}." but I don't have a strand for it ?\n";
		print "We were trying to look into $info_file\n";
		die("\n");
	}
	# %match_coords=();
	my @tab_tr=split("",$tr_seq);
	my @tab_vr=split("",$vr_seq);
	for (my $i=0;$i<=$#tab_tr;$i++){
# 			my $line="";
		my $pos_tr;
		my $pos_vr;
		if ($type eq "plus"){
			$pos_tr=$tab_tr_coord[0]+$i;
			$pos_vr=$tab_vr_coord[0]+$i;
		}
		elsif($type eq "minus"){
			$pos_tr=$tab_tr_coord[1]-$i;
			$pos_vr=$tab_vr_coord[1]-$i;
		}
		$$info_contig{"check_TR"}{$pos_tr}=1;
		$$info_contig{"check_VR"}{$pos_vr}=1;
		$$info_contig{"type_VR"}{$pos_vr}=$repeat;
		if($tab_tr[$i] eq "a" || $tab_tr[$i] eq "A"){
			if ($tab_vr[$i] eq "-"){} ## Don't select if it's a gap in the VR
			else{
				$$info_contig{"check_A_VR"}{$pos_vr}=1;
				# print "$pos_vr in the VR is an A in the TR\n";
			}
		}
	}
	print "The type is $type for $repeat\n";
	print "Now taking info for PnPs on this VR\n";
	my $frame=0;
	if ($type eq "plus"){
		my $start=$$info_contig{"genes"}{$$info_contig{"TRVR"}{$repeat}{"Target_gene"}}{"start"};
		my $diff=$tab_vr_coord[0]-$start;
		if ($diff<0){
			## The VR start before the gene, we have to shift for a little more
			$frame = -1*$diff-1;
		}
		else{
			$frame=$diff % 3;
			if ($frame>0){$frame=3-$frame} ## $frame is actually the number of extra base, but what we need is to shift enough to get back to a number where % 3 == 0, so we need to add the correct complement if frame > 0
		}
		print "We look at VR ".join(":",@tab_vr_coord)." on gene ".$$info_contig{"TRVR"}{$repeat}{"Target_gene"}." which is on strand ".$type." starting at ".$start." so the diff is ".$diff." and the frame is ".$frame."\n";
	}
	elsif($type eq "minus"){
		my $start=$$info_contig{"genes"}{$$info_contig{"TRVR"}{$repeat}{"Target_gene"}}{"stop"};
		my $diff=$start-$tab_vr_coord[1];
		if ($diff<0){$diff=0;}
		$frame=$diff % 3;
		if ($frame>0){$frame=3-$frame} ## $frame is actually the number of extra base, but what we need is to shift enough to get back to a number where % 3 == 0, so we need to add the correct complement if frame > 0
		print "We look at VR ".join(":",@tab_vr_coord)." on gene ".$$info_contig{"TRVR"}{$repeat}{"Target_gene"}." which is on strand ".$type." starting at ".$start." so the diff is ".$diff." and the frame is ".$frame."\n";
	}

	my $total_syn=0;
	my $total_nsyn=0;
	my $total_syn_a=0;
	my $total_nsyn_a=0;
	for (my $i=0;$i<$frame;$i++){
		my $pos_vr="NA";
		if ($type eq "plus"){
			$pos_vr=$tab_vr_coord[0]+$i;
		}
		elsif($type eq "minus"){
			$pos_vr=$tab_vr_coord[1]-$i;
		}
		$check_edge{$pos_vr}=1;
	}
	for (my $i=$frame;$i<=$#tab_vr;$i+=3){ ## We start at frame, i.e. we ignore the first 2 positions if not a full codon
		my $cod=substr($vr_seq,$i,3);
		print "$i - $cod\n";
		if (length($cod)<3){
			print "End of the VR, we don't take partial codons from here\n";
			for (my $j=0;$j<=1;$j++){
				my $pos_vr="NA";
				if ($type eq "plus"){
					$pos_vr=$tab_vr_coord[0]+$i+$j;
				}
				elsif($type eq "minus"){
					$pos_vr=$tab_vr_coord[1]-$i-$j;
				}
				$check_edge{$pos_vr}=1;
			}
			next;
		}
		for (my $j=0;$j<=2;$j++){
			$total_syn+=$$syn_sites{$cod}{$j}{"s"};
			$total_nsyn+=$$syn_sites{$cod}{$j}{"n"};
			my $pos_vr="NA";
			if ($type eq "plus"){
				$pos_vr=$tab_vr_coord[0]+$i+$j;
			}
			elsif($type eq "minus"){
				$pos_vr=$tab_vr_coord[1]-$i-$j;
			}
			if ($$info_contig{"check_A_VR"}{$pos_vr}==1){
				$total_syn_a+=$$syn_sites{$cod}{$j}{"s"};
				$total_nsyn_a+=$$syn_sites{$cod}{$j}{"n"};
				print $pos_vr." is a A in the TR / ".$cod." / ".$j." / ".$$syn_sites{$cod}{$j}{"s"}." / ".$$syn_sites{$cod}{$j}{"n"}."\n";
				# <STDIN>;
				$cod=~tr/atcgn/ATCGN/;
				$match_coords{"A_to_codon"}{$pos_vr}{$$info_contig{"TRVR"}{$repeat}{"Target_gene"}}=$cod;
				print $pos_vr."\t".$cod." -- match_coords\n";
			}
		}
	}
	my $expected_ratio="NA";
	if ($total_syn>0){
		$expected_ratio=$total_nsyn/$total_syn;
	}
	my $expected_ratio_a="NA";
	if ($total_syn_a>0){
		$expected_ratio_a=$total_nsyn_a/$total_syn_a;
	}
	$$info_contig{"VR_info"}{$repeat}{"total_syn"}=$total_syn;
	$$info_contig{"VR_info"}{$repeat}{"total_nsyn"}=$total_nsyn;
	$$info_contig{"VR_info"}{$repeat}{"expected_ratio"}=$expected_ratio;
	print "VR_info - $repeat - $total_syn - $total_nsyn - $expected_ratio - $total_syn_a - $total_nsyn_a - $expected_ratio_a\n";
}


sub test_filtered{
	my $bam_file=$_[0];
	open my $bam,"samtools view $bam_file |";
	my $n=0;
	while(<$bam>){
		chomp($_);
		$n++;
	}
	close $bam;
	return($n);
}


sub check_coverage{
	my $cover_file=$_[0];
	my $l=$_[1];
	open my $tsv,"<",$cover_file;
	my $n=0;
	my $total=0;
	while(<$tsv>){
		chomp($_);
		my @t=split("\t",$_);
		$total+=$t[2];
		if ($t[2]>0){$n++;}
	}
	close $tsv;
	my $avg=$total/$l;
	my $cover=$n/$l;
	return($avg,$cover);
}



sub calculated_stdev(){
	my $store_cover=$_[0];
	my $hash_gene=$_[1];
	my $hash_target=$_[2];
	my $repeat_l=$_[3];
	my $n=1000;
	my @tab_cover=();
	my @tmp_cover=();
	my @tab_gene=keys %{$hash_gene};
	print "getting ready to take random fragments of size $repeat_l for coverage variation\n";
	while ($n>0){
# 		print $n."\n";
		@tmp_cover=();
		&fisher_yates_shuffle(\@tab_gene);
		while($$hash_target{$tab_gene[0]}==1){
			&fisher_yates_shuffle(\@tab_gene);
# 			print "\t\t$tab_gene[0] is a target, we skip\n";
		}
		my $start=int($$hash_gene{$tab_gene[0]}{"start"}+rand()*($$hash_gene{$tab_gene[0]}{"length"}-$repeat_l-1));
		if ($start<1){$start=1;}
# 		print "\t".$tab_gene[0]."\t".$$hash_gene{$tab_gene[0]}{"start"}."\t".$$hash_gene{$tab_gene[0]}{"length"}."\n";
# 		print "\t\t$start -> $repeat_l\n";
		for (my $i=$start;$i<=($start+$repeat_l);$i++){
			if (!defined($$store_cover{$i}) || $$store_cover{$i} eq ""){print "pblm -> $i has no coverage\n";}
			else{
	# 			print "\t\t\t$i\t".$$store_cover{$i}."\n";
				push(@tmp_cover,$$store_cover{$i});
			}
		}
# 		print "\t\t ".join(";",@tmp_cover)."\n";
		my $med=&median(\@tmp_cover);
# 		print "\t\t => ".$med."\n";
		push(@tab_cover,&median(\@tmp_cover));
# 		print "\t\t => ".$tab_cover[$#tab_cover]."\n";
		$n--;
	}
# 	print "Ok, we have all the things we need, now making the computation\n";
	my $median=&median(\@tab_cover);
	my $stdev=&stdev(\@tab_cover);
	return($median,$stdev);
}


sub realign{
	my $seq_1=$_[0];
	my $seq_2=$_[1];
	my $out_file="Tmp.fna";
	my $out_file_ali="Tmp_ali.fna";
	open my $s1,">",$out_file;
	print $s1 ">TR\n";
	print $s1 $seq_1."\n";
	print $s1 ">VR\n";
	print $s1 $seq_2."\n";
	close $s1;
	&run_cmd("muscle -in $out_file -out $out_file_ali");
	my $ali_tr="";
	my $ali_vr="";
	my $c_c="";
	open my $fna,"<",$out_file_ali;
	while(<$fna>){
		chomp($_);
		if ($_=~/^>(\S+)/){$c_c=$1;}
		else{
			if ($c_c eq "TR"){$ali_tr.=$_;}
			if ($c_c eq "VR"){$ali_vr.=$_;}
		}
	}
	close $fna;
	&run_cmd("rm $out_file $out_file_ali");
	if ($ali_vr=~/-/){
		my $new_tr="";
		my $new_vr="";
		my @tab_tr=split("",$ali_tr);
		my @tab_vr=split("",$ali_vr);
		for (my $i=0;$i<=$#tab_vr;$i++){
			if ($tab_vr[$i] ne "-"){
				$new_tr.=$tab_tr[$i];
				$new_vr.=$tab_vr[$i];
			}
		}
		$ali_tr=$new_tr;
		$ali_vr=$new_vr;
	}

	return($ali_tr,$ali_vr);
}

sub run_cmd{
	my $cmd=$_[0];
	my $out=`$cmd`;
	if ($_[1] ne "quiet" && $_[1] ne "veryquiet"){
		if ($_[1] eq "stderr"){print STDERR "$out\n";}
		else{print "$out\n";}
	}
	return($out);
}

sub fisher_yates_shuffle {
	my $deck = shift;  # $deck is a reference to an array
	return unless @$deck; # must not be empty!
	my $i = @$deck;
	while (--$i) {
		my $j = int rand ($i+1);
		@$deck[$i,$j] = @$deck[$j,$i];
	}
}

sub median{
        my($data) = @_;
        if(@$data == 1){
                return 0;
        }
        my $med="NA";
        @$data=sort {$a <=> $b} (@$data);
	if (scalar(@$data) % 2 ==0 ){
		$med=(@{$data}[scalar(@$data)/2]+@{$data}[scalar(@$data)/2-1])/2;
	}
	else{
		$med=@{$data}[int(scalar(@$data)/2)];
	}
        return $med;
}

sub revcomp(){
	my $nuc=$_[0];
	$nuc=~tr/atcguryswkmbdhvn/tagcayrswmkvhdbn/;
	$nuc=~tr/ATCGURYSWKMBDHVN/TAGCAYRSWMKVHDBN/;
	$nuc=reverse($nuc);
	return $nuc;
}

sub stdev{
        my($data) = @_;
        if(@$data == 1){
                return 0;
        }
        my $average = &average($data);
        my $sqtotal = 0;
        foreach(@$data) {
                $sqtotal += ($average-$_) ** 2;
        }
        my $std = ($sqtotal / (@$data-1)) ** 0.5;
        return $std;
}

sub average{
        my($data) = @_;
        if (not @$data) {
                die("Empty arrayn");
        }
        my $total = 0;
        foreach (@$data) {
                $total += $_;
        }
        my $average = $total / @$data;
        return $average;
}

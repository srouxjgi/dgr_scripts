#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Bio::Seq;
use Cwd;
my $h='';
my $cmd='';
my $out='';
my $n_cpu=8;
my $wdir='';
my $dgr_code='';
my $merged_reads_1='';
my $path_filterbam='';
# my $path_bbmap='bbmap.sh';
my $bbmap_path='shifter -i bryce911/bbtools bbmap.sh';
my $samtools_path='samtools';
GetOptions ('help' => \$h, 'h' => \$h, 'd=s'=>\$wdir, 'i=s'=>\$dgr_code, 'r=s'=>\$merged_reads_1, 't=s'=>\$n_cpu, 'p=s'=>\$path_filterbam);
if ($h==1 || $wdir eq "" || $dgr_code eq '' || $merged_reads_1 eq '' || $path_filterbam eq ''){ # If asked for help or did not set up any argument
	print "# Script to map reads to a putative DGR locus
# -d: working directory
# -i: DGR code
# -r: read file R1 (fastq.gz)
# -t : number of CPUs (default: 8)
# -p: path to filterBan
";
	die "\n";
}
if (!($wdir=~/\/$/)){$wdir.="/";}

## Setup everything
my $info_file=$wdir.$dgr_code."_info.ts";
my $ref_fna_ori=$wdir.$dgr_code."_selected.fna";
my $bam_dir=$wdir.$dgr_code."/";
if (!(-d $bam_dir)){&run_cmd("mkdir $bam_dir");}
my $ref_fna=$bam_dir.$dgr_code."_selected.fna";
if (!(-e $ref_fna)){&run_cmd("cp $ref_fna_ori $ref_fna");}
$merged_reads_1=~/(.*?)([^\/]+)_R1\.fastq.gz/;
my $prefix=$1;
my $taxon_id=$2;
my $merged_reads_2=$prefix.$taxon_id."_R2.fastq.gz";
if (!(-e $merged_reads_1)){die("Pblm, $merged_reads_1 doesn't seem to exist\n");}
if (!(-e $merged_reads_2)){die("Pblm, $merged_reads_2 doesn't seem to exist\n");}

my $bam_file=$bam_dir.$taxon_id.".bam";
my $test=$bam_dir.$dgr_code."_selected.fna.bwt";
if (!(-e $test)){
	&run_cmd("bwa index $ref_fna","stderr");
}
if (!(-e $bam_file)){
	&run_cmd("bwa mem -t $n_cpu $ref_fna $merged_reads_1 $merged_reads_2 | samtools view -b -F 4 - | samtools calmd -b - $ref_fna > $bam_file");
}
my $new_filtered=$bam_dir.$taxon_id."_cover50.bam";
if (!(-e $new_filtered)){&run_cmd("$path_filterbam --in $bam_file --out $new_filtered --minId 0 --minCover 50","stderr");}
my $realigned=$bam_dir.$taxon_id."_cover50_globali.bam";
my $fna_ref=$wdir.$dgr_code."_selected.fna";
if (!(-e $fna_ref)){print "$fna_ref doesn't exist ? We can't move forward\n"; next;}
if (!(-e $realigned)){&run_cmd("$bbmap_path in=$new_filtered ref=$fna_ref out=$realigned vslow minid=0 indelfilter=2 inslenfilter=3 dellenfilter=3 mappedonly=t threads=$n_cpu nodisk=t -Xmx120g -eoom");}
if (!(-e $realigned)){print "Pblm, $realigned was not created ??\n"; next;}
my $sorted_globali=$bam_dir.$taxon_id."_cover50_globali_sorted.bam";
if (!(-e $sorted_globali)){
	&run_cmd("$samtools_path sort -@ $n_cpu -o $sorted_globali $realigned");
	&run_cmd("$samtools_path index $sorted_globali");
}
else{
	print "$bam_file is weird, I don't want to touch it\n";
}


sub run_cmd{
	my $cmd=$_[0];
	my $out=`$cmd`;
	if ($_[1] ne "quiet" && $_[1] ne "veryquiet"){
		if ($_[1] eq "stderr"){print STDERR "$out\n";}
		else{print "$out\n";}
	}
	return($out);
}

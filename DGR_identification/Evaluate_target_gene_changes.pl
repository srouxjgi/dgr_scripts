#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Bio::Seq;
use Cwd;
my $h='';
my $cmd='';
my $out='';
my $wdir='';
my $taxon_oid='';
GetOptions ('help' => \$h, 'h' => \$h, 'd=s'=>\$wdir, 'i=s'=>\$taxon_oid);
if ($h==1 || $wdir eq "" || $taxon_oid eq ""){ # If asked for help or did not set up any argument
	print "# Script to look for the target gene in identified TR/VR, and refine (i) which additional VRs are legit, and (ii) what is the exact bias on the coding strand
# Arguments :
# -i : ID of the metagenome (e.g. Metagenome_1) - must be the prefix of all the required files (see README.txt)
# -d : working directory (e.g. DGR_Search_Metagenome_1/) - must be the folder where the required files are stored
";
	die "\n";
}


### Set path to contigs and protein files for this metagenome
my $fna_file=$wdir.$taxon_oid."_contigs.fna";
my $faa_file=$wdir.$taxon_oid."_proteins.faa";
my $gff_file=$wdir.$taxon_oid."_proteins.gff";
if (!(-e $fna_file)){die("Missing $fna_file\n");}
if (!(-e $faa_file)){die("Missing $faa_file\n");}
if (!(-e $gff_file)){die("Missing $gff_file\n");}


$wdir.=$taxon_oid."/";
my $out_file=$wdir."All_RT_1k_DGR_target_detection_filtered.tsv";
my $out_file_faa=$wdir."All_RT_1k_DGR_target_detection_filtered_targets.faa";

if (-e $out_file){
	print "We don't do anything, $out_file is already here\n";
	die("we stop here\n");
}



my %store;
my %check_scaffold;
my $DGR_detection_file=$wdir."All_RT_1k_DGR_detection_filtered.tsv";
open my $tsv,"<",$DGR_detection_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	my $code=$tab[0].":".$tab[2];
	my $tr=$tab[7].":".$tab[8];
	my $vr=$tab[9].":".$tab[10];
	if ($tab[17]==2){
		$tr=$tab[9].":".$tab[10];
		$vr=$tab[7].":".$tab[8];
	}
	if (!defined($store{$code})){
		$store{$code}{"genome"}=$tab[0];
		$store{$code}{"contig"}=$tab[1];
		$check_scaffold{$tab[1]}=1;
		$store{$code}{"original_vr"}=$vr;
	}
	$store{$code}{"by_vr"}{$vr}{"tr"}=$tr;
	$store{$code}{"by_vr"}{$vr}{"code_bias"}=$tab[13];
	$store{$code}{"by_vr"}{$vr}{"shift"}=$tab[6];
	$store{$code}{"by_vr"}{$vr}{"bias"}=$tab[20];
	if ($tab[17]==2){
		$store{$code}{"by_vr"}{$vr}{"bias"}=$tab[21];
	}
	if ($tab[11] eq "plus"){
		$store{$code}{"by_vr"}{$vr}{"tr_strand"}="+";
		$store{$code}{"by_vr"}{$vr}{"vr_strand"}="+";
	}
	elsif ($tab[11] eq "minus") {
		if ($tab[17]==2){
			$store{$code}{"by_vr"}{$vr}{"tr_strand"}="-";
			$store{$code}{"by_vr"}{$vr}{"vr_strand"}="+";
		}
		else{
			$store{$code}{"by_vr"}{$vr}{"tr_strand"}="+";
			$store{$code}{"by_vr"}{$vr}{"vr_strand"}="-";
		}
	}
	else{
		print "## Problem with $tab[11]\n";
# 		<STDIN>;
	}
}
close $tsv;


### Load sequence from relevant contigs
my %store_gene;
my $max_items=50;
my $n_items=0;
my @tab_queries;
my $query="";

my %store_contig_seq;
my $tag=0;
my $c_c="";
open my $fna,"<",$fna_file;
while(<$fna>){
	chomp($_);
	if ($_=~/^>(\S+)/){
		$c_c=$1;
		$tag=0;
		if (defined($check_scaffold{$c_c})){
			$tag=1;
		}
	}
	elsif($tag==1){
		$store_contig_seq{$c_c}.=$_;
	}
}
close $fna;

### Load information on relevant genes
my %store_gene_all;
my %check_gene;
open my $gff,"<",$gff_file;
while(<$gff>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	my $contig=$tab[0];
	$tab[8]=~/ID=([^;]+)/;
	my $tmp=$1;
	$tmp=~/.*_(\d+)$/;
	my $gene_id=$tab[0]."_".$1; ## matching gene ID to prodigal format
	my $product="Unknown";
	if ($tab[8]=~/Product=([^;]+)/){$product=$1;}
	if (defined($check_scaffold{$contig})==1){
		$check_gene{$gene_id}=$contig;
		$store_gene_all{$contig}{$gene_id}{"type"}=$tab[2];
		$store_gene_all{$contig}{$gene_id}{"start"}=$tab[3]-1; ## Adjust coordinate to match the ones from BLAST, etc ## WITH IMG, NEEDED A -1
		$store_gene_all{$contig}{$gene_id}{"stop"}=$tab[4]-1;
		$store_gene_all{$contig}{$gene_id}{"strand"}=$tab[6];
		$store_gene_all{$contig}{$gene_id}{"product"}=$product;
	}

}
close $gff;

$tag=0;
$c_c="";
open my $fna,"<",$faa_file;
while(<$fna>){
	chomp($_);
	if ($_=~/^>(\S+)/){
		$c_c=$1;
		$tag=0;
		if (defined($check_gene{$c_c})){
			$tag=1;
		}
	}
	elsif($tag==1){
		$store_gene_all{$check_gene{$c_c}}{$c_c}{"aa"}.=$_;
	}
}
close $fna;


open my $s1,">",$out_file;
print $s1 "## Candidate DGR code\tSelected VR\tTarget type\tAdjusted bias (A;T;C;G)\tTarget\tTarget product\tAdjusted TR sequence\tAdjusted VR sequence\tAdjusted VR amino acid sequence\tVR start\tVR end\tTarget start\tTarget stop\tVR position within Target\tFull target sequence\tFull VR-Target overlap\n";
open my $s2,">",$out_file_faa;
## For each RT
foreach my $code (sort keys %store){
	# Load genes from this contig
	print "\n\n\n";
	my $contig=$store{$code}{"contig"};
	my @tmp=split(":",$code);
	my $RT_gene=$tmp[1];
	print "Working on $contig -- RT gene is $RT_gene\n";
	# Load fna of the contig
	my $contig_seq=$store_contig_seq{$contig};
	if ($contig_seq eq ""){die("pblm, no contig sequence for $contig");}
	my %store_gene=%{$store_gene_all{$contig}};

	foreach my $selected_vr (sort keys %{$store{$code}{"by_vr"}}){
		print "Looking for genes linked to $selected_vr\n";
		my $type="secondary";
		if ($selected_vr eq $store{$code}{"original_vr"}){$type="primary";}
		my @t_vr=split(":",$selected_vr);
		my $start_vr=$t_vr[0]+$store{$code}{"by_vr"}{$selected_vr}{"shift"}-1;
		my $end_vr=$t_vr[1]+$store{$code}{"by_vr"}{$selected_vr}{"shift"}-1;
		my $seq_vr=substr($contig_seq,$start_vr,($end_vr-$start_vr+1));

		my @t_tr=split(":",$store{$code}{"by_vr"}{$selected_vr}{"tr"});
		my $start_tr=$t_tr[0]+$store{$code}{"by_vr"}{$selected_vr}{"shift"}-1;
		my $end_tr=$t_tr[1]+$store{$code}{"by_vr"}{$selected_vr}{"shift"}-1;
		my $seq_tr=substr($contig_seq,$start_tr,($end_tr-$start_tr+1));


		my $bias=$store{$code}{"by_vr"}{$selected_vr}{"bias"};
		my $code_bias=$store{$code}{"by_vr"}{$selected_vr}{"code_bias"};
		# Look at strand, target gene, translation (?) to get codon / amino acid change, and establish the true VR (which strand it is) and the true bias
		foreach my $gene_id (sort keys %store_gene){
			if ($gene_id eq $RT_gene){
				print "$gene_id is $RT_gene, we don't consider as a credible target\n";
				next;
			}
			print "We check $gene_id - $start_vr and $end_vr vs ".$store_gene{$gene_id}{"start"}." and ".$store_gene{$gene_id}{"stop"}."\n";
			if (($start_vr > $store_gene{$gene_id}{"start"} && $start_vr < $store_gene{$gene_id}{"stop"}) || ($end_vr > $store_gene{$gene_id}{"start"} && $end_vr < $store_gene{$gene_id}{"stop"})){
	# 			print "### FOUND ONE\n";
				####### COPY OVER THE DATA TO NOT OVERWRITE THEM WHEN MULTIPLE POTENTIAL GENE FOR SAME VR !
				my $start_vr_adj=$start_vr;
				my $end_vr_adj=$end_vr;
				my $start_tr_adj=$start_tr;
				my $end_tr_adj=$end_tr;
				my $seq_vr_adj=$seq_vr;
				my $seq_tr_adj=$seq_tr;
				###
				my $pos_vr=0;
				my $seq_gene=substr($contig_seq,$store_gene{$gene_id}{"start"},($store_gene{$gene_id}{"stop"}-$store_gene{$gene_id}{"start"}+1));
				if ($store_gene{$gene_id}{"strand"} eq "-"){$seq_gene=&revcomp($seq_gene);}
				if ($store_gene{$gene_id}{"annot"} eq ""){$store_gene{$gene_id}{"annot"}="hypothetical protein";}
# 				print "\n\n";
				print "Selected VR $code is from $start_vr_adj to $end_vr_adj , with bias ".$code_bias."\n";
				print "TR is : $start_tr - $end_tr while VR is: $start_vr - $end_vr (before adjustment)\n";
				print "TR - VR strands are ".$store{$code}{"by_vr"}{$selected_vr}{"tr_strand"}." and ".$store{$code}{"by_vr"}{$selected_vr}{"vr_strand"}."\n";
				print "Shift is ".$store{$code}{"by_vr"}{$selected_vr}{"shift"}."\n";
				print "Match to ".$gene_id."\t".$store_gene{$gene_id}{"start"}."\t".$store_gene{$gene_id}{"stop"}."\t".$store_gene{$gene_id}{"strand"}."\t---".$store_gene{$gene_id}{"annot"}."---\n";
# 				<STDIN>;
				# Check if the gene is partial and maybe we should adjust its coordinates
				my $diff=$store_gene{$gene_id}{"stop"}-$store_gene{$gene_id}{"start"}+1;
				if ($diff % 3 == 0){}## All right
				else{
					print "This gene is likely partial - diff $diff\n";
					my $length_total=length($contig_seq);
					my $rest=$diff % 3;
					if ($store_gene{$gene_id}{"start"} == 0){
						print "\t start is 1, so we switch it to the correct frame\n";
						print "\t from ".$store_gene{$gene_id}{"start"}."\n";
						$store_gene{$gene_id}{"start"}+=$rest;
						print "\t to ".$store_gene{$gene_id}{"start"}."\n";
					}
					elsif ($store_gene{$gene_id}{"stop"} == ($length_total-1)){
						print "\t stop is length of contig , so we change it to the correct frame\n";
						print "\t from ".$store_gene{$gene_id}{"stop"}."\n";
						$store_gene{$gene_id}{"stop"}-=$rest;
						print "\t to ".$store_gene{$gene_id}{"stop"}."\n";
					}
					else{
						print "### This is confusing, I have a diff of $diff with the following internal gene\n";
						print "0 ".$store_gene{$gene_id}{"start"}." ".$store_gene{$gene_id}{"stop"}." ".$length_total."\n";
						print "in doubt, we will move the start position based on the strand, but that's not necessarily what we should do, we're just unsure, \n";
						if ($store_gene{$gene_id}{"strand"} eq "+"){
							print "\t from ".$store_gene{$gene_id}{"start"}."\n";
							$store_gene{$gene_id}{"start"}+=$rest;
							print "\t to ".$store_gene{$gene_id}{"start"}."\n";
						}
						else{
							print "\t from ".$store_gene{$gene_id}{"stop"}."\n";
							$store_gene{$gene_id}{"stop"}-=$rest;
							print "\t to ".$store_gene{$gene_id}{"stop"}."\n";
						}
					}
				}
				## We truncate if the overlap is partial
				if ($start_vr_adj < $store_gene{$gene_id}{"start"}){
					print " %%% Adjust start %%% $start_vr_adj vs ".$store_gene{$gene_id}{"start"}."\n";
					my $diff=$store_gene{$gene_id}{"start"}-$start_vr_adj;
					$start_vr_adj=$store_gene{$gene_id}{"start"};
					$seq_vr_adj=substr($contig_seq,$start_vr_adj,($end_vr_adj-$start_vr_adj+1));
					$start_tr_adj+=$diff;
					$seq_tr_adj=substr($contig_seq,$start_tr_adj,($end_tr_adj-$start_tr_adj+1));
				}
				if ($end_vr_adj > $store_gene{$gene_id}{"stop"}){
					print " %%% Adjust end %%% $end_vr_adj vs ".$store_gene{$gene_id}{"stop"}."\n";
					my $diff=$end_vr_adj-$store_gene{$gene_id}{"stop"};
					$end_vr_adj=$store_gene{$gene_id}{"stop"};
					$seq_vr_adj=substr($contig_seq,$start_vr_adj,($end_vr_adj-$start_vr_adj+1));
					$end_tr_adj-=$diff;
					$seq_tr_adj=substr($contig_seq,$start_tr_adj,($end_tr_adj-$start_tr_adj+1));
				}
				print "Adjusted VR is $start_vr_adj to $end_vr_adj\n";
				if (($end_vr_adj-$start_vr_adj) < 9){
					print "### This looks too short, less than 3 amino acids, we don't consider\n";
					next;
				}

				## Go load the protein sequence of this gene
				## Right now, seq_vr_adj and seq_tr_adj are both from the plus strand
				my $seq_aa=$store_gene{$gene_id}{"aa"};
				print $s2 ">".$gene_id."\n".$seq_aa."\n";
				my $tag=0;
				if ($store_gene{$gene_id}{"strand"} eq "+"){
					print "Gene in plus strand, we don't really have to adjust, unless the VR is in minus\n";
					## Seq_vr_adj is from the plus strand, which is the gene strand, so we don't want to change anything
					if ($store{$code}{"by_vr"}{$selected_vr}{"vr_strand"} eq "-"){
						print "VR used to be in minus, so we may have to change the TR\n";
						## The blast was to VR minus. If it was minus-minus, we are good, because right now we have plus-plus  (VR-TR)
						## But if the blast used to be minus-plus, then we need to RC the TR to get a plus-minus  (VR-TR)
						if ($store{$code}{"by_vr"}{$selected_vr}{"tr_strand"} eq "+"){
							print "TR also used to be in plus but is actually in minus, so we reverse complement and change the bias\n";
							$seq_tr_adj=&revcomp($seq_tr_adj);
							my @tmp=split(";",$bias);
							$bias=$tmp[1].";".$tmp[0].";".$tmp[3].";".$tmp[2];
						}
					}
					elsif($store{$code}{"by_vr"}{$selected_vr}{"tr_strand"} eq "-"){
						## This is the case where we have VR plus: if this was plus-plus then we are good
						## But if the blast is plus-minus, then we have to RC tr so that it goes from plus to minus
						print "TR used to be in minus, so we have to RC\n";
						$seq_tr_adj=&revcomp($seq_tr_adj);
					}
				}
				else{
					print "Gene in minus strand, we adjust everything\n";
					## Seq_vr_adj is from the plus strand, which is not the gene strand, so we have to RC it anyway
					if ($store{$code}{"by_vr"}{$selected_vr}{"vr_strand"} eq "+"){
						## this is the complex situtation, where we definitely have to RC VR, and the blast used to be either plus-plus or plus-minus (VR-TR)
						print "VR used to be in plus, we reverse complement since it's actually in minus\n";
						$seq_vr_adj=&revcomp($seq_vr_adj);
						## If the blast used to be plus-plus, both TR/VR on same strand, you have to RC everything and change the bias
						if ($store{$code}{"by_vr"}{$selected_vr}{"tr_strand"} eq "+"){
							print "TR also used to be in plus, so we reverse complement but change the bias\n";
							$seq_tr_adj=&revcomp($seq_tr_adj);
							my @tmp=split(";",$bias);
							$bias=$tmp[1].";".$tmp[0].";".$tmp[3].";".$tmp[2];
						}
						else{
							print "TR was opposed to VR and already found in minus, meaning we don't have to RC it because minus of minus is plus, and the bias should be correct as well\n";
						}
					}
					else {
						## We shouldn't have any minus-minus, it should always be minus-plus.
						## if it's minus-plus, then we have to RC VR so that seq_adj_vr is minus
						print "VR was identified on the minus strand, we RC the sequence but everything else should be ok\n";
						$seq_vr_adj=&revcomp($seq_vr_adj);
					}

				}
				# Checking that we adjusted this ok
				if ($seq_gene=~/$seq_vr_adj/){} ## Everything ok, the adjusted vr should be part of the gene
				else{
					print "###!!!!### THIS IS UNEXPECTED\n";
					print "Here is gene vs vr\n";
					print $seq_gene."\n";
					print "\n";
					print $seq_vr_adj."\n";
					print "?\n";
# 					<STDIN>;
				}
				## Checking the translation
				# Calculating the frame
				my $frame=0;
				if ($store_gene{$gene_id}{"strand"} eq "+"){
					my $diff=($start_vr_adj-$store_gene{$gene_id}{"start"}) % 3;
					if ($diff==0){$frame=0;} ## In the right frame
					elsif ($diff==1){$frame=2;} ## One "hanging" aa, have to add 2 more to go to the correct frame
					elsif ($diff==2){$frame=1;} ## Two "hanging" aas, have to add 1 more to go to the correct frame
					else{die("diff frame + is $diff - should not be possible");}
					print "Gene is in strand ".$store_gene{$gene_id}{"strand"}." and start at ".$store_gene{$gene_id}{"start"}." for a vr which starts at ".$start_vr_adj." so diff is $diff and frame is $frame\n";
					$pos_vr=$start_vr_adj-$store_gene{$gene_id}{"start"};
				}
				elsif ($store_gene{$gene_id}{"strand"} eq "-"){
					my $diff=($store_gene{$gene_id}{"stop"}-$end_vr_adj) % 3;
					if ($diff==0){$frame=0;} ## In the right frame
					elsif ($diff==1){$frame=2;} ## One "hanging" aa, have to add 2 more to go to the correct frame
					elsif ($diff==2){$frame=1;} ## Two "hanging" aas, have to add 1 more to go to the correct frame
					print "Gene is in strand ".$store_gene{$gene_id}{"strand"}." and start at ".$store_gene{$gene_id}{"stop"}." for a vr which starts at ".$end_vr_adj." so diff is $diff and frame is $frame\n";
					$pos_vr=$store_gene{$gene_id}{"stop"}-$end_vr_adj;
				}
				else{
					print "You shouldn't be here\n";
					print $store_gene{$gene_id}{"strand"}."\n";
	# 				<STDIN>;
				}
				my $seq_bio = Bio::Seq->new(-id => "dummy", -seq =>$seq_vr_adj,-alphabet => 'dna' );
				my @seqs = Bio::SeqUtils->translate_6frames($seq_bio);
				my $prot_vr=$seqs[$frame]->seq;
				$seq_bio = Bio::Seq->new(-id => "dummy", -seq =>$seq_tr_adj,-alphabet => 'dna' );
				@seqs = Bio::SeqUtils->translate_6frames($seq_bio);
				my $prot_tr=$seqs[$frame]->seq;
				my $tag_ok=0;
				print "### TR - VR - bias\n";
				print $seq_tr_adj."\n";
				print $seq_vr_adj."\n";
				print $bias."\n";
				print $prot_tr."\n";
				print $prot_vr."\n";
				my $prot_vr_regex=$prot_vr;
				$prot_vr_regex=~s/\*//g;
				if ($seq_aa=~/(.*)$prot_vr_regex(.*)/){
					print $1." ---- ".$prot_vr_regex." ---- ".$2."\n";
					$tag_ok=1;
				}
				elsif($seq_aa=~/^M/ && $prot_vr=~/^[VL]/){
					## Sometimes alternative start is translated as M instead of V or L by BioPerl, we try to solve this automatically
					$seq_aa=~/^M(.*)/;
					my $seq_aa_trunc=$1;
					$prot_vr_regex=~/^[VL](.*)/;
					my $prot_vr_trunc=$1;
					if ($seq_aa=~/$prot_vr_trunc(.*)/){
						print $prot_vr_trunc." --- ".$1."\n";
						$tag_ok=1;
					}
				}
				if ($tag_ok == 0){
					print "###!!!### Pblm with prot sequence\n";
					print $seq_aa."\n";
					print "### NOT SURE WHAT HAPPENS HERE\n";
					print $seq_gene."\n";
					my $seq_bio = Bio::Seq->new(-id => "dummy", -seq =>$seq_vr_adj,-alphabet => 'dna' );
					my @seqs = Bio::SeqUtils->translate_6frames($seq_bio);
					for (my $i=0;$i<=5;$i++){
						print $seqs[$i]->seq."\n";
					}
					print "########\n";
					if ($store_gene{$gene_id}{"strand"} eq "+"){
						my $frag_before_vr=substr($seq_gene,0,($start_vr_adj-$store_gene{$gene_id}{"start"}));
						my $seq_bio = Bio::Seq->new(-id => "dummy", -seq =>$frag_before_vr,-alphabet => 'dna' );
						my $prot_before="";
						if ($frag_before_vr ne ""){
							my @seqs = Bio::SeqUtils->translate_6frames($seq_bio);
							$prot_before=$seqs[0]->seq;
						}
						my $frag_after_vr=substr($seq_gene,$end_vr_adj,$store_gene{$gene_id}{"stop"}-$end_vr_adj);
						my $prot_after="dummy";

						print $frag_before_vr." ---- ".$seq_vr_adj." ---- ".$frag_after_vr."\n";
						print $prot_before." ---- ".$prot_vr." ----". $prot_after."\n";
					}
					$tag_ok=1;
				}
				else{
					## calculate the position at which the VR is found as a ratio of gene length
					$pos_vr/=($store_gene{$gene_id}{"stop"}-$store_gene{$gene_id}{"start"});
					print $s1 $code."\t".$selected_vr."\t".$type."\t".$bias."\t".$gene_id."\t".$store_gene{$gene_id}{"annot"}."\t".
					$seq_tr_adj."\t".$seq_vr_adj."\t".$prot_vr."\t".$start_vr_adj."\t".$end_vr_adj."\t".$store_gene{$gene_id}{"start"}."\t".
					$store_gene{$gene_id}{"stop"}."\t".$pos_vr."\t".$seq_aa."\t".$tag_ok."\n";
				}
				print "-------------\n";
			}
		}
	}
}
close $s1;
close $s2;

sub revcomp(){
	my $nuc=$_[0];
	$nuc=~tr/atcguryswkmbdhvn/tagcayrswmkvhdbn/;
	$nuc=~tr/ATCGURYSWKMBDHVN/TAGCAYRSWMKVHDBN/;
	$nuc=reverse($nuc);
	return $nuc;
}


sub test_avail{
	my $to_test=$_[0];
	my $path=&run_cmd("which ".$to_test);
	if ($path eq ""){die("pblm, we did not find the $to_test path, was the program installed and/or the right environment/module loaded ?\n");}
}

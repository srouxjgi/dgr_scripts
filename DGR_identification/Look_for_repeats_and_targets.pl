#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Cwd;
my $h='';
my $cmd='';
my $out='';
my $n_cpu=8;
my $wdir='';
my $taxon_oid='';
GetOptions ('help' => \$h, 'h' => \$h, 'd=s'=>\$wdir, 'i=s'=>\$taxon_oid , 't=s'=>\$n_cpu);
if ($h==1 || $wdir eq "" || $taxon_oid eq ""){ # If asked for help or did not set up any argument
	print "# Script to look for repeats that could be interesting for a potential DGR
# Arguments :
# -i : ID of the metagenome (e.g. Metagenome_1) - must be the prefix of all the required files (see README.txt)
# -d : working directory (e.g. DGR_Search_Metagenome_1/) - must be the folder where the required files are stored
# -t: number of threads
";
	die "\n";
}

&test_avail("blastn");

if (!($wdir=~/\/$/)){$wdir.="/";}

### Set path to contigs and protein files for this metagenome
my $fna_file=$wdir.$taxon_oid."_contigs.fna";
my $faa_file=$wdir.$taxon_oid."_proteins.faa";
my $gff_file=$wdir.$taxon_oid."_proteins.gff";
if (!(-e $fna_file)){die("Missing $fna_file\n");}
if (!(-e $faa_file)){die("Missing $faa_file\n");}
if (!(-e $gff_file)){die("Missing $gff_file\n");}

$wdir.=$taxon_oid."/";

my $in_file=$wdir."List_RT.tsv";
my $out_file=$wdir."All_RT_1k_repeat_detection.tsv";
if (-e $out_file){
	print "We rm $out_file to start fresh\n";
	&run_cmd("rm $out_file");
}

my $tmp_dir=$wdir."Tmp_dir/";
if (!(-d $tmp_dir)){&run_cmd("mkdir $tmp_dir");}

## List candidates
my %list_candidates;
open my $tsv,"<",$in_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	if ($tab[3]>=1000 && $tab[4]>1 && $tab[0] eq $taxon_oid){
		$list_candidates{$tab[0]}{$tab[2]}{$tab[5]}=1;
		print $tab[0]."\t".$tab[2]."\t".$tab[5]."\n";
	}
}
close $tsv;


my $tmp=$tmp_dir."/tmp.fasta";
my $tmp_db=$tmp_dir."/tmpdb";
my $out_blast=$tmp_dir."/tmp-vs-itself.tab";
open my $final_out,">",$out_file;
print $final_out "## Genome\tContig\tCandidate RT\tRT start\tRT stop\tRT strand\tFragment_start\tQstart\tQend\tSstart\tSend\tStrand\tN sub\tN gap\tBias Q (A;T;C;G)\tBias S (A;T;C;G)\tSeq Q\tSeq S\tDistance from end to predicted IMH\tIMH Q\tIMH S\n";
my %check_contig;
my %store_seq;
## Prep variables for db searches
my @tab_queries;
my $max_items=50;
my $query='';
my $n_items=0;

print "Looking into $taxon_oid ...\n";
if (-e $tmp){
	&run_cmd("rm $tmp_dir/tmp*");
}
%check_contig=();
%store_seq=();
my @tab_scaffolds=sort keys %{$list_candidates{$taxon_oid}};
## List all coordinates of candidates
my @tab_candidates;
foreach my $scaffold (@tab_scaffolds){
	foreach my $cand (sort keys %{$list_candidates{$taxon_oid}{$scaffold}}){
		push(@tab_candidates,$cand);
	}
}

open my $gff,"<",$gff_file;
while(<$gff>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	my $contig=$tab[0];
	$tab[8]=~/ID=([^;]+)/;
	my $tmp=$1;
	$tmp=~/.*_(\d+)$/;
	my $gene_id=$tab[0]."_".$1; ## matching gene ID to prodigal format
	if ($list_candidates{$taxon_oid}{$contig}{$gene_id}==1){
		$check_contig{$contig}{$gene_id}{"rt_start"}=$tab[3];
		$check_contig{$contig}{$gene_id}{"rt_end"}=$tab[4];
		$check_contig{$contig}{$gene_id}{"rt_strand"}=$tab[6]; ## To check
		$check_contig{$contig}{$gene_id}{"start"}=$tab[3]-20000; ## we start 20k upstream of RT
		$check_contig{$contig}{$gene_id}{"window_length"}=$tab[4]-$tab[3]+1+40000; # Calculate window length by adding the gene + 20k downstream
		print "$_\n";
	}
}
close $gff;

my $tag=0;
my $c_c="";
my $c_seq="";
open my $fna,"<",$fna_file;
while(<$fna>){
	chomp($_);
	if ($_=~/^>(\S+)/){
		$tag=0;
		my $id=$1;
		if (defined($check_contig{$c_c})){
			foreach my $cand (sort keys %{$list_candidates{$taxon_oid}{$c_c}}){
				if (!defined($check_contig{$c_c}{$cand}{"start"})){
					print "###### PBLM, WE DON'T HAVE A START FOR $cand\n";
					die("triple argh\n");
				}
				if ($check_contig{$c_c}{$cand}{"start"}<0){$check_contig{$c_c}{$cand}{"start"}=0;}
				$store_seq{$c_c}{$cand}=substr($c_seq,$check_contig{$c_c}{$cand}{"start"},$check_contig{$c_c}{$cand}{"window_length"});
			}
		}
		$c_c=$id;
		$c_seq="";
	}
	else{
		$c_seq.=$_;
	}
}
close $fna;
## Don't forget the last one
if (defined($check_contig{$c_c})){
	foreach my $cand (sort keys %{$list_candidates{$taxon_oid}{$c_c}}){
		if (!defined($check_contig{$c_c}{$cand}{"start"})){
			print "###### PBLM, WE DON'T HAVE A START FOR $cand\n";
			die("triple argh\n");
		}
		if ($check_contig{$c_c}{$cand}{"start"}<0){$check_contig{$c_c}{$cand}{"start"}=0;}
		$store_seq{$c_c}{$cand}=substr($c_seq,$check_contig{$c_c}{$cand}{"start"},$check_contig{$c_c}{$cand}{"window_length"});
	}
}

## Process each of these to find putatively interesting repeats
foreach my $contig (sort keys %store_seq){
	print "## Looking into $contig\n";
	foreach my $cand (sort keys %{$store_seq{$contig}}){
		print "\t looking into $cand\n";
		open my $s1,">",$tmp;
		print $s1 ">".$contig."\n".$store_seq{$contig}{$cand}."\n";
		close $s1;
		&run_cmd("makeblastdb -in $tmp -out $tmp_db -dbtype nucl");
		&run_cmd("blastn -task blastn -db $tmp_db -query $tmp -out - -outfmt \"6 qseqid sseqid qstart qend sstart send evalue bitscore score sstrand pident positive qseq sseq\" -max_target_seqs 100 -word_size 8 -dust no -gapopen 6 -gapextend 2 | gawk -F '\\t' '{if (\$1==\$2){if (\$3==\$5 && \$4==\$6){}else{print \$0}}}' > $out_blast");
		my @result=split("\n",&parse_blast($out_blast));
		foreach my $line (@result){
			print $final_out $taxon_oid."\t".$contig."\t".$cand."\t".$check_contig{$contig}{$cand}{"rt_start"}."\t".$check_contig{$contig}{$cand}{"rt_end"}."\t".$check_contig{$contig}{$cand}{"rt_strand"}
			."\t".$check_contig{$contig}{$cand}{"start"}."\t".$line."\n";
		}
		$check_contig{$contig}{$cand}{"done"}=1;
	}
}
## Double check we found all the sequences we were looking for
print "Final check\n";
foreach my $contig (keys %check_contig){
	foreach my $cand (keys %{$check_contig{$contig}}){
		if ($check_contig{$contig}{$cand}{"done"}==1){}
		else{
			print "#### PBLM --- $contig -- $cand\n";
			die("\n");
		}
	}
}
# 	$i++;
print "done\n";
close $final_out;
print "Output file $out_file created\n";

my $repeat_file=$wdir."All_RT_1k_repeat_detection.tsv";
my $out_file=$wdir."All_RT_1k_DGR_detection.tsv";

my $th_bias_1=0.5;
my $th_bias_2=0.8;


my %store;
my $c_code="";
open my $tsv,"<",$repeat_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	my $code=$tab[0]."-".$tab[1]."-".$tab[2];
	if ($code ne $c_code){
		if ($c_code=~/^NA-NA-\[t160\]/){<STDIN>;}
		print "\n\n\n## Checking a candidate for $tab[0] - $tab[1] - $tab[2]\n";
		$c_code=$code;
	}
	print "--------- new candidate --------- $tab[7]:$tab[8] -- $tab[9]:$tab[10]\n";
	# First loading data from the best candidate so far, if any
	my $b_bias=0;
	my $b_bias_at=0;
	my $b_imh=0;
	my $b_gap=10;
	my $b_dist=40000;
	if (defined($store{$code})){
		$b_gap=$store{$code}{"gap"};
		$b_bias=$store{$code}{"bias_code"};
		$b_bias_at=$store{$code}{"bias_code_at"};
		$b_imh=$store{$code}{"imh_length"};
		$b_dist=$store{$code}{"tr_dist"};
	}
	print "Best candidate has $b_gap - $b_bias - $b_imh and $b_dist\n";
	# First, we exclude if there are no substitutions
	my $n_sub=$tab[12];
	if ($n_sub <= 0){
		print "We have 0 substitutions, we don't think it's possible\n";
		next;
	}
	# Now we have a real candidate, so we check biases towards 1 nucleotide, and A/T in general: if we have a better candidate with a bias, we don't take this new one, note that we exclude bias calculations from < 5 substitutions
	my @bias_total=split(";",$tab[14]);
# 	print join("-",@bias_total)."\n";
	my $bias_a_q=$bias_total[0]/$n_sub;
	my $bias_t_q=$bias_total[1]/$n_sub;
	push(@bias_total,split(";",$tab[15]));
# 	print join("-",@bias_total)."\n";
	my $bias_a_s=$bias_total[4]/$n_sub;
	my $bias_t_s=$bias_total[5]/$n_sub;
# 	print $bias_total[4]."\t".$bias_total[5]."\n";
	@bias_total=sort {$b <=> $a} @bias_total;
	my $c_bias=$bias_total[0]/$n_sub;
	my @bias_at=sort {$b <=> $a} ($bias_a_q,$bias_t_q,$bias_a_s,$bias_t_s);
	my $c_bias_at=$bias_at[0];
	print "$tab[14] - $tab[15] vs $tab[12] -> my max is $bias_total[0], my bias is $c_bias, AT bias is $c_bias_at\n";
# 	print $bias_a_q."\t".$bias_t_q."\t".$bias_a_s."\t".$bias_t_s."\n";
	my $code_bias=0;
	if ($c_bias >= $th_bias_1){$code_bias=1;}
	if ($c_bias >= $th_bias_2){$code_bias=2;}
	my $code_bias_at=0;
	if ($c_bias_at >= $th_bias_1){$code_bias_at=1;}
	if ($c_bias_at >= $th_bias_2){$code_bias_at=2;}
	if ($n_sub <= 5 ){
		print "We have $n_sub substitutions, we can't really calculate a bias...\n";
		$code_bias=0;
		$code_bias_at=0;
	}
	# Now we calculate how close the TR/VR is from the RT, and exclude if > 1000
	my $min_dist=40000;
	my $tr_num=0;
	my $start_test=7;
	my $end_test=10;
	if ($c_bias >= $th_bias_1){
		print "We have some bias, we try to check if we can identify the TR from here\n";
		my $best_q=0;
		my $best_s=0;
		if ($c_bias_at > $th_bias_1 ){
			print "\t we'll do this from the AT bias\n";
			## Look at AT bias since we have some
			if ($bias_a_q > $best_q){$best_q=$bias_a_q;}
			if ($bias_t_q > $best_q){$best_q=$bias_t_q;}
			if ($bias_a_s > $best_s){$best_s=$bias_a_s;}
			if ($bias_t_s > $best_s){$best_s=$bias_t_s;}
		}
		else {
			print "\t no AT bias, we'll do this from a general bias\n";
			## Look at bias outside of AT
			my @bias_q=sort { $b <=> $a} split(";",$tab[14]);
			$best_q=$bias_q[0]/$n_sub;
			my @bias_s=sort { $b <=> $a} split(";",$tab[15]);
			$best_s=$bias_s[0]/$n_sub;
		}
		my $bias_q_code=0;
		if ($best_q >= $th_bias_1){$bias_q_code=1;}
		if ($best_q >= $th_bias_2){$bias_q_code=2;}
		my $bias_s_code=0;
		if ($best_s >= $th_bias_1){$bias_s_code=1;}
		if ($best_s >= $th_bias_2){$bias_s_code=2;}
		print "Bias in query - $best_q == $bias_q_code vs bias in subject - $best_s == $bias_s_code\n";
		if ($bias_q_code > $bias_s_code){
			$end_test=8;
			print "\t clearly TR is the query, so we will only look at distance between RT and query\n";
			# We'll only look in the query, i.e. tab[7] to tab[8]
		}
		elsif ($bias_s_code > $bias_q_code){
			$start_test=9;
			print "\t clearly TR is the subject, so we will only look at distance between RT and subject\n";
			# We'll only look in the subject, i.e. tab[9] to tab[10]
		}

	}
	for (my $i=$start_test;$i<=$end_test;$i++){
		my $dist_a=abs($tab[$i]+$tab[6]-$tab[3]);
		if ($dist_a < $min_dist){
			$min_dist=$dist_a;
			if ($i==7 || $i==8){$tr_num=1;}
			elsif ($i==9 || $i==10){$tr_num=2;}
		}
		my $dist_b=abs($tab[$i]+$tab[6]-$tab[4]);
		if ($dist_b < $min_dist){
			$min_dist=$dist_b;
			if ($i==7 || $i==8){$tr_num=1;}
			elsif ($i==9 || $i==10){$tr_num=2;}
		}
	}
	# We look at IMH prediction
	my $imh_length_code=0;
	if ($tab[19] ne "NA"){
		$imh_length_code=1;
		if(length($tab[19])>=10){
			$imh_length_code=2;
		}
	}
	# And the number of gap
	my $n_gap=$tab[13];
	if ($n_gap eq ""){$n_gap=0;}
	# Now comparing to the best candidate
	print "comparing to best --- New value vs Best\n";
	print "A/T bias: ".$code_bias_at." vs ".$b_bias_at."\n";
	print "Bias: ".$code_bias." vs ".$b_bias."\n";
	print "IMH: ".$imh_length_code." vs ".$b_imh."\n";
	print "Gap: ".$n_gap." vs ".$b_gap."\n";
	print "TR dist: ".$min_dist." vs ".$b_dist."\n";
	if ($b_bias_at > $code_bias_at){
		print "We have an A/T bias worse than the best candidate so we don't take the new one\n";
		next;
	}
	elsif($code_bias_at > $b_bias_at){
		print "We have a better A/T bias, we take no matter what\n";
	}
	else{
		# Tied on AT bias, looking at overall bias
		if ($b_bias > $code_bias && $code_bias_at==0){
			print "We have no A/T bias and a single nucleotide bias worse than the best candidate so we don't take the new one\n";
			next;
		}
		elsif($code_bias > $b_bias && $code_bias_at==0){
			print "We have no A/T bias and a single nucleotide bias better than the best candidate so we take the new one\n";
		}
		else{
			# If we're here, we have the same A/T bias, and same no-AT bias (if no AT bias), so we look at IMH
			if ($b_imh > $imh_length_code){
				print "We have a comparable candidate with a much better imh, we don't take the new one\n";
				next;
			}
			elsif($imh_length_code > $b_imh){
				print "We have a better imh, bias is equivalent, we take the new one\n";
			}
			else{
				# At this point we are tied on bias and imh status, so break the tie based on gaps
				if ($n_gap > 0 && $b_gap == 0){
					print "We have some gap and the best candidate doesn't , we don't take the new one\n";
					next;
				}
				elsif (($b_gap % 3 == 0) && ($n_gap % 3 > 0)){
					print "We have a potential fraemshift in the new one, but not in the best candidate, we don't take the new one\n";
					next;
				}
				elsif ($b_gap < $n_gap){
					print "We have equivalent gaps but more gaps in the new one, so we don't take the new one\n";
					next;
				}
				else{
					# At this point everything is equivalent, so we take the one closest to the TR
					if ($b_dist < $min_dist){
						print "We have a very comparable candidate but closer to the RT, we skip\n";
						next;
					}
				}
			}
		}
	}
	print "We think this is the new best candidate ! \n";
	$store{$code}{"gap"}=$n_gap;
	$store{$code}{"tr_dist"}=$min_dist;
	$store{$code}{"tr_num"}=$tr_num;
	$store{$code}{"bias_code"}=$code_bias;
	$store{$code}{"bias_level"}=sprintf("%.3f",$c_bias);
	$store{$code}{"bias_code_at"}=$code_bias;
	$store{$code}{"bias_level_at"}=sprintf("%.3f",$c_bias_at);
	$store{$code}{"imh_length"}=$imh_length_code;
	$store{$code}{"line"}=$tab[0]."\t".$tab[1]."\t".$tab[2]."\t".$tab[3]."\t".$tab[4]."\t".$tab[5]."\t".$tab[6]."\t".$tab[7]."\t".
	$tab[8]."\t".$tab[9]."\t".$tab[10]."\t".$tab[11]."\t".$n_gap."\t".$code_bias."\t".$store{$code}{"bias_level"}."\t".$store{$code}{"bias_level_at"}."\t".
	$min_dist."\t".$tr_num."\t".$imh_length_code."\t".$tab[19].";".$tab[20]."\t".$tab[14]."\t".$tab[15];

	print $store{$code}{"gap"}."\t".$store{$code}{"tr_dist"}."\t".$store{$code}{"tr_num"}."\t".$store{$code}{"bias_code"}."\t".
	$store{$code}{"bias_level"}."\t".$store{$code}{"bias_code_at"}."\t".$store{$code}{"bias_level_at"}."\t".$store{$code}{"imh"}."\n";
	print $store{$code}{"line"}."\n";
	my $tr_coords=$tab[7].":".$tab[8];
	my $vr_coords=$tab[9].":".$tab[10];
	if ($tr_num == 2){
		$tr_coords=$tab[9].":".$tab[10];
		$vr_coords=$tab[7].":".$tab[8];
	}
	$store{$code}{"tr_coords"}=$tr_coords;
	$store{$code}{"vr_coords"}=$vr_coords;
}
close $tsv;
## Re-read to take additional ones from the same TR if existing
$c_code="";

print "############# Now looking at additional VRs #############\n";
my $n_positive=0;
open my $s1,">",$out_file;
print $s1 "## (Meta)Genome\tContig\tRT gene\tRT start\tRT stop\tRT strand\tFragment start\tQuery Start\tQuery Stop\tSubject Start\tSubject Stop\tMatch strand\tN gaps\tBias code\tMax Single nucleotide bias\tMax A/T bias\tMinimum distance to RT\tTR Q or S\tIMH length code\tPredicted IMH\tMismatches query (A;T;C;G)\tMismatches subject (A;T;C;G)\n";
open my $tsv,"<",$repeat_file;
my %seen;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){next;}
	my @tab=split("\t",$_);
	my $code=$tab[0]."-".$tab[1]."-".$tab[2];
	if (!defined($store{$code})){next;}
	if (!defined($seen{$code})){
		print "### Looking at $code\n";
		# First time we see $code on this loop, we print to the out_file
		print $s1 $store{$code}{"line"}."\n";
		$n_positive++;
		$seen{$code}=1;
	}
	my $new_tr=$tab[7].":".$tab[8];
	my $new_vr=$tab[9].":".$tab[10];
	if (($store{$code}{"tr_coords"} eq $new_tr && $store{$code}{"vr_coords"} eq $new_vr) || ($store{$code}{"vr_coords"} eq $new_tr && $store{$code}{"tr_coords"} eq $new_vr)){
		# same prediction, we skip
		next;
	}
	my @t_tr=split(":",$store{$code}{"tr_coords"});
	my $ratio_overlap_tr=&check_overlap_length($t_tr[0],$t_tr[1],$tab[7],$tab[8])/($t_tr[1]-$t_tr[0]+1);
	my $ratio_overlap_vr=&check_overlap_length($t_tr[0],$t_tr[1],$tab[9],$tab[10])/($t_tr[1]-$t_tr[0]+1);
	print "Comparing $new_tr / $new_vr to ".$store{$code}{"tr_coords"}." (".$store{$code}{"vr_coords"}.")\n";
	print $ratio_overlap_tr."\n";
	print $ratio_overlap_vr."\n";

	if ($ratio_overlap_tr>=0.5 || $ratio_overlap_vr>=0.5){
		print "We have found the same tr ".$store{$code}{"tr_coords"}." with a different match\n";
		print "Best match: ".$store{$code}{"tr_coords"}." - ".$store{$code}{"vr_coords"}."\n";
		print "New match: ".$new_tr." - ".$new_vr."\n";
		my $tr_num=1;
		if ($ratio_overlap_vr>$ratio_overlap_tr){$tr_num=2;}
		## Calculate what you need to calculate
		my $imh_length_code=0;
		if ($tab[19] ne "NA"){
			$imh_length_code=1;
			if(length($tab[19])>=10){
				$imh_length_code=2;
			}
		}
		my $n_sub=$tab[12];
		my $c_bias=0;
		my $c_bias_at=0;
		my $code_bias=0;
		my $code_bias_at=0;
		if ($n_sub <= 5 ){
			print "We have $n_sub substitutions, we can't really calculate a bias...\n";
		}
		else{
			my @bias_total=split(";",$tab[14]);
			my $bias_a_q=$bias_total[0]/$n_sub;
			my $bias_t_q=$bias_total[1]/$n_sub;
			push(@bias_total,split(";",$tab[15]));
			my $bias_a_s=$bias_total[4]/$n_sub;
			my $bias_t_s=$bias_total[5]/$n_sub;
			@bias_total=sort {$b <=> $a} @bias_total;
			$c_bias=$bias_total[0]/$n_sub;
			my @bias_at=sort {$b <=> $a} ($bias_a_q,$bias_t_q,$bias_a_s,$bias_t_s);
			$c_bias_at=$bias_at[0];
			print "$tab[14] - $tab[15] vs $tab[12] -> my max is $bias_total[0], my bias is $c_bias, AT bias is $c_bias_at\n";
			if ($c_bias >= $th_bias_1){$code_bias=1;}
			if ($c_bias >= $th_bias_2){$code_bias=2;}
			if ($c_bias_at >= $th_bias_1){$code_bias_at=1;}
			if ($c_bias_at >= $th_bias_2){$code_bias_at=2;}
		}
		my $min_dist=40000;
		my $start=7;
		my $stop=8;
		if ($tr_num == 2){
			$start=9;
			$stop=10;
		}
		for (my $i=$start;$i<=$stop;$i++){
			my $dist_a=abs($tab[$i]+$tab[6]-$tab[3]);
			if ($dist_a < $min_dist){
				$min_dist=$dist_a;
			}
			my $dist_b=abs($tab[$i]+$tab[6]-$tab[4]);
			if ($dist_b < $min_dist){
				$min_dist=$dist_b;
			}
		}
		print $s1 $tab[0]."\t".$tab[1]."\t".$tab[2]."\t".$tab[3]."\t".$tab[4]."\t".$tab[5]."\t".$tab[6]."\t".$tab[7]."\t".$tab[8]."\t".
		$tab[9]."\t".$tab[10]."\t".$tab[11]."\t".$tab[13]."\t".$code_bias."\t".sprintf("%.3f",$c_bias)."\t".sprintf("%.3f",$c_bias_at)."\t".
		$min_dist."\t".$tr_num."\t".$imh_length_code."\t".$tab[19].";".$tab[20]."\t".$tab[14]."\t".$tab[15]."\n";
		$n_positive++;
		print $tab[0]."\t".$tab[1]."\t".$tab[2]."\t".$tab[3]."\t".$tab[4]."\t".$tab[5]."\t".$tab[6]."\t".$tab[7]."\t".$tab[8]."\t".
		$tab[9]."\t".$tab[10]."\t".$tab[11]."\t".$tab[13]."\t".$code_bias."\t".sprintf("%.3f",$c_bias)."\t".sprintf("%.3f",$c_bias_at)."\t".
		$min_dist."\t".$tr_num."\t".$imh_length_code."\t".$tab[19].";".$tab[20]."\t".$tab[14]."\t".$tab[15]."\n";
# 		<STDIN>;
	}
	if ($code=~/^NA-NA-\[t028\]/){<STDIN>;}
}
close $s1;

### Now filtering more strictly with specific criteria
### Current criteria
## RT protein length is ≥ 150 and ≤ 650
## TR is ≥ 9bp long
## RT - TR distance is ≤ 1kb
my $min_length=150;
my $max_length=650;
my $in_file=$wdir."All_RT_1k_DGR_detection.tsv";
my $out_file=$wdir."All_RT_1k_DGR_detection_filtered.tsv";
my %count;
open my $s1,">",$out_file;
open my $tsv,"<",$in_file;
while(<$tsv>){
	chomp($_);
	if ($_=~/^#/){print $s1 $_."\n";next;}
	my @tab=split("\t",$_);
	my $length=$tab[4]-$tab[3]+1;
	$length/=3; ## To get the aa
	my $min_dist=$tab[16];
	my $length_repeat=$tab[8]-$tab[7]+1;
	my $code=$tab[0].":".$tab[2];
	print "### ".$code." --> ".$length."\t".$min_dist."\t".$length_repeat."\n";
	if ($length < $min_length){print "$code is too short, we don't take\n"; $count{"short_RT"}++; next;}
	if ($length > $max_length){print "$code is too long, we don't take\n"; $count{"long_RT"}++; next;}
	if ($length_repeat < 9){print "Repeat is too short, we don't take\n"; $count{"short_repeat"}++; next;} ## This is now useless, we now have a 50 cutoff on repeat length earlier,
	if ($min_dist > 1000){print "RT-TR dist is too long, we don't take\n"; $count{"TR_too_far"}++; next;}
	$count{"all_ok"}++;
	print $s1 $_."\n";
}
close $tsv;
close $s1;

foreach my $key (sort keys %count){
	print $key."\t".$count{$key}."\n";
}



sub parse_blast{
	my $in_blast=$_[0];
	my $out="";
	open my $tsv,"<",$in_blast;
	while(<$tsv>){
		chomp($_);
		my @tab=split("\t",$_);
		my $l1=abs($tab[3]-$tab[2]+1);
		my $l2=abs($tab[5]-$tab[4]+1);
		## Switch over if ever we are on a plus to minus hit and thus the subject coordinates are RCed
		if ($tab[5]<$tab[4]){
			my $tmp=$tab[5];
			$tab[5]=$tab[4];
			$tab[4]=$tmp;
		}
		if ($tab[10]>99){ ## OR 90 ?? We use to have it at 90
			print "We have a hit at $tab[10]%, we exclude because there is not enough variation there\n";
		}
		elsif ($l1 < 50 || $l2 < 50){
			print "We have hits of length $l1 and $l2 nucleotides, it's too short, we don't consider\n";
		}
		elsif($tab[4]<$tab[2]){
			print "We don't look at $tab[2]-$tab[3] and $tab[4]-$tab[5], wait for the 'reverse' in the 5'-3' orientation\n";
		}
		elsif(&check_overlap($tab[2],$tab[3],$tab[4],$tab[5])==1){
			print "$tab[2]-$tab[3] and $tab[4]-$tab[5] => These overlap, we don't think it's possible for TRs/VRs\n";
		}
		else{
			print "We have a possible hit == $tab[10]\n";
			my ($n_sub,$n_gap,$bias_in,$bias_out)=&get_bias($tab[12],$tab[13]);
			my ($coord,$seq_a,$seq_b,$mismatches,$ratio)=&get_imh($tab[12],$tab[13]);
			########### LOOK INTO THE LAST 20 NUCLEOTIDES FOR AN IMH (GC -RICH 9 to 20 bp)
			$out.=$tab[2]."\t".$tab[3]."\t".$tab[4]."\t".$tab[5]."\t".$tab[9]."\t".$n_sub."\t".$n_gap."\t".$bias_in."\t".$bias_out."\t".
			$tab[12]."\t".$tab[13]."\t".$coord."\t".$seq_a."\t".$seq_b."\t".$mismatches."\t".$ratio."\n";
			print $tab[2]."\t".$tab[3]."\t".$tab[4]."\t".$tab[5]."\t".$tab[9]."\t".$n_sub."\t".$n_gap."\t".$bias_in."\t".$bias_out."\t".
			$tab[12]."\t".$tab[13]."\t".$coord."\t".$seq_a."\t".$seq_b."\t".$mismatches."\t".$ratio."\n";
		}
	}
	close $tsv;
	return ($out);
}


sub check_overlap{
	my $qstart=$_[0];
	my $qend=$_[1];
	my $sstart=$_[2];
	my $send=$_[3];
	my $return=0;
	if ($sstart>=$qstart && $sstart<=$qend){$return=1;}
	elsif($send>=$qstart && $send<=$qend){$return=1;}
	elsif($sstart<$qstart && $send>$qend){$return=1;}
	return $return;
}

sub check_overlap_length{
	my $qstart=$_[0];
	my $qend=$_[1];
	my $sstart=$_[2];
	my $send=$_[3];
	my $return=0;
	if ($sstart>=$qstart && $sstart<=$qend){
		$return=$qend-$sstart;
		if ($send < $qend){$return=$send-$sstart;}
	}
	elsif($send>=$qstart && $send<=$qend){
		$return=$send-$qstart;
		if ($qstart < $sstart){$return=$send-$sstart;}
	}
	elsif($sstart<$qstart && $send>$qend){
		$return=$qend-$qstart;
	}
	return $return;
}

sub get_imh{
	$_[0]=~tr/atcgnx/ATCGNX/;
	$_[1]=~tr/atcgnx/ATCGNX/;
	my @t_a=split("",$_[0]);
	my @t_b=split("",$_[1]);
	@t_a=reverse(@t_a);
	@t_b=reverse(@t_b);
	my $tag=0;
	my %count;
	my %store;
	for (my $i=0;$i<20;$i++){
		%count=();
		$count{"matches"}=0;
		$count{"mismatches"}=0;
		my $j=$i+5;
		for (my $k=$i;$k<=$j;$k++){
			$count{"a"}{"total"}++;
			if ($t_a[$k] eq "G" || $t_a[$k] eq "C"){
				$count{"a"}{"GC"}++;
			}
			$count{"b"}{"total"}++;
			if ($t_b[$k] eq "G" || $t_b[$k] eq "C"){
				$count{"b"}{"GC"}++;
			}
			if ($t_b[$k] eq $t_a[$k]){
				$count{"matches"}++;
			}
			else{
				$count{"mismatches"}++;
			}
		}
		if ($count{"a"}{"GC"}/$count{"a"}{"total"} >= 0.66 && $count{"b"}{"GC"}/$count{"b"}{"total"} >= 0.66 && $count{"mismatches"}<=1){
# 			print "This is promising, trying to extend \n";
			$j++;
			while($t_b[$j] eq $t_a[$j] && ($t_b[$j] eq "G" || $t_b[$j] eq "C") && ($t_a[$j] eq "G" || $t_a[$j] eq "C")){
# 				print "We extend by one\n";
				$count{"matches"}++;
				$count{"a"}{"GC"}++;
				$count{"a"}{"total"}++;
				$count{"b"}{"GC"}++;
				$count{"b"}{"total"}++;
				$j++;
			}
			$j--;
			### Check if store and/or replaced
			my $ratio_a=$count{"a"}{"GC"}/$count{"a"}{"total"};
			my $ratio_b=$count{"b"}{"GC"}/$count{"b"}{"total"};
			my $ratio=$ratio_a;
			if ($ratio_b<$ratio_a){$ratio=$ratio_b;}
			my $mismatch=$count{"mismatches"};
			my $length=$j-$i+1;
			if (!defined($store{"ratio"}) || $ratio>$store{"ratio"} || ($ratio == $store{"ratio"} && $mismatch > $store{"mismatch"}) || ($ratio == $store{"ratio"} && $mismatch == $store{"mismatch"} && $length > $store{"length"})){
				$store{"start"}=$i;
				$store{"ratio"}=$ratio;
				$store{"length"}=$length;
				$store{"match"}=$count{"matches"};
				$store{"mismatches"}=$mismatch;
				$store{"seq_a"}=reverse(join("",@t_a[$i..$j]));
				$store{"seq_b"}=reverse(join("",@t_b[$i..$j]));
			}
		}
	}
	if (defined($store{"start"})){
		return ($store{"start"},$store{"seq_a"},$store{"seq_b"},$store{"mismatches"},sprintf("%.3f",$store{"ratio"}));
	}
	else{
		return ("NA","NA","NA","NA","NA");
	}
}



sub get_bias{
	my @t_a=split("",$_[0]);
	my @t_b=split("",$_[1]);
	my $n_sub=0;
	my $n_gap=0;
	my %count;
	# initialize to get 0s even if no detection
	$count{"in"}{"A"}=0;
	$count{"in"}{"T"}=0;
	$count{"in"}{"C"}=0;
	$count{"in"}{"G"}=0;
	$count{"out"}{"A"}=0;
	$count{"out"}{"T"}=0;
	$count{"out"}{"C"}=0;
	$count{"out"}{"G"}=0;
	my $i=0;
	for ($i=0;$i<=$#t_a;$i++){
		if (!defined($t_a[$i])){print "###### PBLM, NOTHING at $i for TA $_[0]\n";}
		if (!defined($t_b[$i])){print "###### PBLM, NOTHING at $i for TB $_[1]\n";}
		$t_a[$i]=~tr/atcgnx/ATCGNX/;
		$t_b[$i]=~tr/atcgnx/ATCGNX/;
		if ($t_a[$i] eq $t_b[$i]){}
		else{
			if ($t_a[$i] eq "-"){
				$count{"in"}{"gap"}++;
			}
			elsif ($t_b[$i] eq "-"){
				$count{"out"}{"gap"}++;
# 				if ($t_a[$i] ne "-"){$count{"in"}{$t_a[$i]}++;}
# 				elsif ($t_b[$i] ne "-"){$count{"out"}{$t_b[$i]}++;}
			}
			else{
				$n_sub++;
				$count{"in"}{$t_a[$i]}++;
				$count{"out"}{$t_b[$i]}++;
			}
		}
	}
	if ($count{"in"}{"gap"} > 0 ){$n_gap=$count{"in"}{"gap"};}
	if ($count{"out"}{"gap"} > $count{"in"}{"gap"}){
		if ($count{"out"}{"gap"} % 3 == 0){
			# We don't replace if the original is not %3 = 0 because it's a red flag we want to keep
			if ($n_gap % 3 == 0){
				$n_gap=$count{"out"}{"gap"};
			}
		}
		else{
			$n_gap=$count{"out"}{"gap"};
		}
	}
	if (defined($t_a[$i])){print "######ooo### PBLM, SOMETHING at $i for TA $_[0]\n";}
	if (defined($t_b[$i])){print "######ooo### PBLM, SOMETHING at $i for TB $_[1]\n";}
	my $bias_in=$count{"in"}{"A"}.";".$count{"in"}{"T"}.";".$count{"in"}{"C"}.";".$count{"in"}{"G"};
	my $bias_out=$count{"out"}{"A"}.";".$count{"out"}{"T"}.";".$count{"out"}{"C"}.";".$count{"out"}{"G"};
	return ($n_sub,$n_gap,$bias_in,$bias_out);
}

sub run_cmd{
	my $cmd=$_[0];
	my $out=`$cmd`;
	if ($_[1] ne "quiet" && $_[1] ne "veryquiet"){
		if ($_[1] eq "stderr"){print STDERR "$out\n";}
		else{print "$out\n";}
	}
	return($out);
}

sub test_avail{
	my $to_test=$_[0];
	my $path=&run_cmd("which ".$to_test);
	if ($path eq ""){die("pblm, we did not find the $to_test path, was the program installed and/or the right environment/module loaded ?\n");}
}

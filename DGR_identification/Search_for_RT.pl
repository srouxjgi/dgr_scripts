#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Cwd;
my $h='';
my $cmd='';
my $out='';
my $taxon_oid='';
my $out_dir="";
my $n_cpu=8;
my $custom_db='Custom_DGR_RT.hmm';
my $done_file='List_done';
GetOptions ('help' => \$h, 'h' => \$h, 'i=s'=>\$taxon_oid, 'd=s'=>\$out_dir, 't=s'=>\$n_cpu);
if ($h==1 || $taxon_oid eq "" || $out_dir eq ""){ # If asked for help or did not set up any argument
	print "# Script to search for a DGR in a metagenome
# Arguments:
# -i : ID of the metagenome (e.g. Metagenome_1) - must be the prefix of all the required files (see README.txt)
# -d : working directory (e.g. DGR_Search_Metagenome_1/) - must be the folder where the required files are stored
# -t: number of threads\n";
	die "\n";
}

&test_avail("hmmsearch");

if (!($out_dir=~/\/$/)){$out_dir.="/";}

### Set path to contigs and protein files for this metagenome
my $fna_file=$out_dir.$taxon_oid."_contigs.fna";
my $faa_file=$out_dir.$taxon_oid."_proteins.faa";
if (!(-e $fna_file)){die("Missing $fna_file\n");}
if (!(-e $faa_file)){die("Missing $faa_file\n");}

### Setup everything if needed, and stop if result already there
my $wdir=$out_dir.$taxon_oid."/";
if (!(-d $wdir)){&run_cmd("mkdir $wdir");}
my $tmp_dir=$wdir."Tmp_dir/";
if (!(-d $tmp_dir)){&run_cmd("mkdir $tmp_dir");}
my $final_file=$wdir."List_RT.tsv";
if (-e $final_file){die("$final_file already there, we skip\n");}

## Take all prots from contigs > 1kb -> put in a tmp fasta file
my $out_file_prot_tmp=$tmp_dir."All_prots_1kb_tmp.faa";

print "load relevant contigs length\n";
my %info_contig;
my $c_c="";
my $tag=0;
open my $fna,"<",$fna_file;
while(<$fna>){
      chomp($_);
      if ($_=~/^>(\S+)/){$c_c=$1;}
      else{$info_contig{$c_c}{"length"}+=length($_);}
}
close $fna;

if (!(-e $out_file_prot_tmp)){
	print "##### We create $out_file_prot_tmp #### \n";
	open my $s1,">",$out_file_prot_tmp;
      $tag=0;
	open my $faa,"<",$faa_file;
      while(<$faa>){
            chomp($_);
            if ($_=~/^>(\S+)/){
                  $tag=0;
                  my $prot_id=$1;
                  $prot_id=~/(.*)_\d+$/;
                  my $contig_id=$1;
                  if ($info_contig{$contig_id}{"length"}>=1000){
                        $tag=1;
                        print $s1 $_."\n";
                  }
            }
            elsif($tag==1){
                  print $s1 $_."\n";
            }
      }
	close $s1;

}
## If no relevant prot, we skip
if (!(-e $out_file_prot_tmp)){
	&run_cmd("echo 'no prot file  (?)' > $done_file");
# 	&run_cmd("rm -r $wdir");
	die("\n");
}

my %store_len;
$tag=0;
my $n_prot=0;
open my $fa,"<",$out_file_prot_tmp;
while(<$fa>){
	chomp($_);
	if ($_=~/^>(\S+)/){
		$c_c=$1;
		$n_prot++;
            $c_c=~/(.*)_\d+$/;
            my $contig_id=$1;
            $info_contig{$contig_id}{"n_genes"}++;
	}
	else{
		$store_len{$c_c}+=length($_);
	}
}
close $fa;

## Run hmmsearch
print "#### Running the HMMsearch if needed ####\n";
my $out_hmm=$wdir."All_prots_1kb_tmp-vs-custom_hmm.tab";
if (!(-e $out_hmm)){&run_cmd("hmmsearch -o /dev/null --noali -E 1.0e-05 --tblout $out_hmm --cpu $n_cpu $custom_db $out_file_prot_tmp");}

## Take the list of candidate RTs
print "#### Now parsing the HMM ####\n";
my %check_new;
my $tag_last_line=0;
my $tag_new=0;
open my $tsv,"<",$out_hmm;
while(<$tsv>){
	chomp($_);
	if ($_=~/# \[ok\]/){$tag_last_line=1;}
	if ($_=~/^#/){next;}
	my @tab=split(" ",$_);
	if ($tab[5]>=50 && $tab[4]<=0.00001){
		if (!defined($check_new{$tab[0]}) || ($tab[5] > $check_new{$tab[0]}{"score"})){
			$check_new{$tab[0]}{"select"}=1;
			$check_new{$tab[0]}{"match"}=$tab[2];
			$check_new{$tab[0]}{"evalue"}=$tab[4];
			$check_new{$tab[0]}{"score"}=$tab[5];
			$tag_new=1;
		}
	}
}
close $tsv;
if ($tag_last_line==0){
	die("SOMETHING WENT WRONG IN THE HMMSEARCH -> $out_hmm\n");
}

if ($tag_new==0){
	print "No RT\n";
	&run_cmd("echo 'done' > $done_file");
	&run_cmd("rm -r $wdir");
}
else{
	print "We have some new RT, we write the info we need\n";
	open my $s1,">",$final_file;
	print $s1 "## Metagenome\tFull contig ID\tContig ID\tLength\tN genes\tGene ID\tDGR Hit\tEvalue\tBit Score\n";
	foreach my $cand (sort keys %check_new){
		if ($check_new{$cand}{"select"}==1){
                  $cand=~/(.*)_\d+$/;
                  my $contig=$1;
			print $s1 $taxon_oid."\t".$taxon_oid."_____".$contig."\t".$contig."\t".$info_contig{$contig}{"length"}."\t".$info_contig{$contig}{"n_genes"}."\t".
                  $cand."\t".$check_new{$cand}{"match"}."\t".$check_new{$cand}{"evalue"}."\t".$check_new{$cand}{"score"}."\n";
		}
	}
	close $s1;
	&run_cmd("rm $out_file_prot_tmp"); ## We get rid of the tmp faa
}

sub run_cmd{
	my $cmd=$_[0];
	my $out=`$cmd`;
	if ($_[1] ne "quiet" && $_[1] ne "veryquiet"){
		if ($_[1] eq "stderr"){print STDERR "$out\n";}
		else{print "$out\n";}
	}
	return($out);
}

sub test_avail{
	my $to_test=$_[0];
	my $path=&run_cmd("which ".$to_test);
	if ($path eq ""){die("pblm, we did not find the $to_test path, was the program installed and/or the right environment/module loaded ?\n");}
}

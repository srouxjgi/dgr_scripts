### Scripts used for DGR analyses in the manuscript "Ecological niches and molecular targets of hypermutation in the global microbiome." (Roux et al., 2021) ###
- DGR_identification: set of scripts used to identify candidate DGRs from genomes and metagenomes
- Diversity_estimation: set of scripts used to perform read mapping and SNV/SAV estimation on DGR loci
- Targets_clustering: script used to generate the protein clusters for DGR targets
- Companion_datasets: fasta files of the RT, DGR Targets, and TR/VR pairs. Targets and TR/VR pairs include all automatic prediction, i.e. before any manual curation.

Each folder includes a Readme file with specific instructions and requirements of each analysis
All tools have been developed and tested on GNU/Linux, and when run on the example data should complete in less than 5 minutes.

****************************

Distribution and activity patterns of targeted hypermutation in the global microbiome
(Scripts_DGR) Copyright (c) 2020, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any required
approvals from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at
IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department
of Energy and the U.S. Government consequently retains certain rights.  As
such, the U.S. Government has been granted for itself and others acting on
its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
Software to reproduce, distribute copies to the public, prepare derivative
works, and perform publicly and display publicly, and to permit others to do so.

****************************

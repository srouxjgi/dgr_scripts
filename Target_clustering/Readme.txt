### How to run:
./Get_target_PCs.pl -i Example/Target_subset.faa -d Example/Target_subset_PCs

### Requirements:
BLAST 2.9.0+
mcl 14-137+
HH-suite3

### Input (examples):
Amino acid fasta file of target proteins (e.g. Target_subset.faa)

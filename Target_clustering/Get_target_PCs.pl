#!/usr/bin/env perl
use strict;
use autodie;
use Getopt::Long;
use Cwd;
my $h='';
my $dir='';
my $wdir='';
my $n_cpu=6;
my $fasta_file='';
my $out_dir='';
GetOptions ('help' => \$h, 'h' => \$h, 'i=s'=>\$fasta_file, 'd=s'=>\$out_dir);
if ($h==1 || $fasta_file eq "" || $out_dir eq ""){ # If asked for help or did not set up any argument
	print "# Script to get the PCs for targets
# Arguments :
# -i : input fasta file
# -d : working directory
";
	die "\n";
}


&test_avail("mcl");
&test_avail("blastp");
&test_avail("hhsuitedb.py");
&test_avail("hhsearch");

my $root="";
if ($fasta_file=~/([^\/]+)\.f[^\.]+$/){$root=$1;}
if ($root eq ""){die("is $fasta_file a fasta file with the correct extension ?\n");}

if (!(-d $out_dir)){&run_cmd("mkdir $out_dir");}
if (!($out_dir=~/\/$/)){$out_dir.="/";}
$root=$out_dir."/".$root;

print "### Loading sequences\n";
my %store_seq;
open my $fa,"<",$fasta_file;
my $c_s="";
my $tag=0;
while(<$fa>){
	chomp($_);
	if ($_=~/^>(\S+)/){
		$tag=0;
		$c_s=$1;
	}
	else{
		$store_seq{$c_s}.=$_;
	}
}
close $fa;

print "### First MCL\n";
my $out_blast=$root."-vs-itself.tab";
if (!(-e $out_blast)){
	my $db_path="$root";
	&run_cmd("makeblastdb -in $fasta_file -out $db_path -dbtype prot");
	&run_cmd("blastp -query $fasta_file -db $db_path -out $out_blast -outfmt \"6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen nident positive\" -evalue 0.001 -num_threads $n_cpu");
}

my $abc_file=$root."-vs-itself.abc";
if (-e $abc_file){print "we use $abc_file\n";}
else{
	my %store;
	open my $tsv,"<",$out_blast;
	while(<$tsv>){
		chomp($_);
		my @tab=split("\t",$_);
		if ($tab[0] eq $tab[1]){next;}
		if ($tab[11]>=50){
			if (!defined($store{"match"}{$tab[0]}{$tab[1]}) || $tab[11]>$store{"match"}{$tab[0]}{$tab[1]}){
				$store{"match"}{$tab[0]}{$tab[1]}=$tab[11];
			}
		}
	}
	close $tsv;
	open my $s1,">",$abc_file;
	foreach my $query (sort keys %{$store{"match"}}){
		foreach my $subject (sort keys %{$store{"match"}{$query}}){
			print $s1 $query."\t".$subject."\t".$store{"match"}{$query}{$subject}."\n";
		}
	}
	close $s1;
}

my $out_file_cluster=$root."-vs-itself.mci.I20";
if (-e $out_file_cluster){print "$out_file_cluster is already here, we'll use it\n";}
else{
	## We cluster
	my $mci_file=$root."-vs-itself.mci";
	my $tab_file=$root."-vs-itself.mcl_tab";
	&run_cmd("mcxload -abc $abc_file --stream-mirror --stream-neg-log10 -stream-tf 'ceil(200)' -o $mci_file -write-tab $tab_file");
	&run_cmd("mcl $mci_file -I 2 -use-tab $tab_file -o $out_file_cluster");
}

print "### Now parsing MCL results\n";
my %prot_to_cluster;
my %cluster_to_prot;
my %store_clusters;
my $out_file_cluster_prot=$root."_prot_to_PC.tsv";
my $out_dir_clusters=$root."_PCs/";
if (!(-d $out_dir_clusters)){&run_cmd("mkdir $out_dir_clusters");}
open my $tsv,"<",$out_file_cluster;
my $i=0;
my $c_c="";
my $log_out="Cluster_writing.log";
open my $s1,">",$out_file_cluster_prot;
while(<$tsv>){
	chomp($_);
	$i++;
	$c_c="PC_".sprintf("%0.5d",$i);
	my @tab=split("\t",$_);
	foreach my $prot (@tab){
		$store_clusters{$c_c}{"nb"}++;
		$store_clusters{$c_c}{"prots"}{$prot}++;
		print $s1 "$c_c\t$prot\n";
	}
	if ($#tab>0){
		print "Creating $c_c files ... \n";
		my $out_file=$out_dir_clusters.$c_c.".faa";
		if (!(-e $out_file)){
			open my $s1_cluster,">",$out_file;
			foreach my $prot (@tab){
				print $s1_cluster ">$prot\n$store_seq{$prot}\n";
			}
			close $s1_cluster;
		}
		my $out_file_ali=$out_dir_clusters.$c_c."_ali.faa";
		if (!(-e $out_file_ali)){`mafft --thread $n_cpu --reorder --auto $out_file > $out_file_ali 2> $log_out`;}
# 			if (!(-e $out_file_ali)){`muscle -in $out_file -out $out_file_ali >> $log_out 2>&1`;}
		my $out_file_ali_a3m=$out_dir_clusters.$c_c."_ali.a3m";
		if (!(-e $out_file_ali_a3m)){`reformat.pl fas a3m $out_file_ali $out_file_ali_a3m -M 50`;}
		&clean_ali_a3m($out_file_ali_a3m,$c_c);
		my $out_hmm=$out_file_ali.".hmm";
		if (!(-e $out_hmm)){`hhmake -add_cons -M 50 -i $out_file_ali -o $out_hmm -name $c_c -diff inf`;}
	}
}
close $tsv;
close $s1;

my @t_c=sort {$store_clusters{$b}{"nb"} <=> $store_clusters{$a}{"nb"} || $a cmp $b } keys %store_clusters;
# print "### Then HHSearch\n";
print "## Creating the HMM db\n";
my $db_file=$root."_all_db_hhm.ffdata";
my $db_path=$root."_all_db";
my $db_path_to_rm=$root."_all_db_";
if (!(-e $db_file)){
	&run_cmd("rm $db_path_to_rm*");
	&run_cmd("hhsuitedb.py --ia3m=$out_dir_clusters/*a3m --ihhm=$out_dir_clusters/*.hmm -o $db_path --cpu 1");
}

my %link;
foreach my $cluster (@t_c){
	my $out_file_ali=$out_dir_clusters.$cluster."_ali.faa";
	if (-e $out_file_ali){
		print "Comparing $cluster ..\n";
		my $out_search=$out_dir_clusters.$cluster.".out_hhsearch";
		if (!(-e $out_search)){`hhsearch -M 50 -add_cons -norealign -hide_pred -hide_dssp -E 0.001 -i $out_file_ali -d $db_path -o $out_search -cpu 8`;}
		open my $ssv,"<",$out_search;
		my $tag=0;
		while(<$ssv>){
			chomp($_);
			if ($tag==0){
				if ($_=~/^ No Hit       /){
					$tag=1;
# 					print "$_ -> Start of the interesting region\n";
				}
			}
			elsif($tag==1 && $_ eq ""){
				$tag=0; # End of the interesting region
			}
			elsif ($tag==1){
# 				print "Interesting region : $_\n";
				my @tab=split(" ",$_);
				my $hit=$tab[1];
				my $prob=$tab[2];
				my $evalue=$tab[3];
				my $length=$tab[10];
				$length=~s/\(//;
				$length=~s/\)//;
				my @t=split("-",$tab[9]);
				my $length_hit=$t[1]-$t[0]+1;
				if ($length==0){
					if ($tab[9]=~/(\d+)-(\d+)\((\d+)\)/){
						$length_hit=$2-$1;
						$length=$3;
					}
					else{
						print "$_\n";
						print "$tab[9] === $tab[10]\n";
						print "This is weird !\n";
						<STDIN>;
					}
				}
				my $cover=$length_hit/$length;
				if (($prob>=90 && $cover>=0.5) || ($prob>=99 && $cover>=0.2 && $length_hit>=100)){
					if ($hit eq $cluster){}
					else{
						print "We find a link between $hit and $cluster -> $tab[0] $tab[1] $tab[2] $tab[3] $tab[5] $cover $length_hit\n";
						$link{$hit}{$cluster}{"score"}=$tab[5];
						$link{$hit}{$cluster}{"evalue"}=$tab[2];
						$link{$hit}{$cluster}{"cover"}=$cover;
						$link{$hit}{$cluster}{"length"}=$length_hit;
					}
				}
			}
		}
		close $ssv;
	}
}

print "## Now doing a greedy clustering\n";
my $i=0;
my %cluster_to_supercluster;
my %store_supercluster;
my @tab_seed;
my $supercluster_file=$root."_superclusters.tsv";
open my $s3,">",$supercluster_file;
foreach my $cluster (@t_c){
	my $tag=0;
	my $sc="";
	my $c_score=0;
	foreach my $seed (@tab_seed){
		if ($link{$cluster}{$seed}{"score"}>0 && $sc eq ""){
			$tag=1;
			$sc=$seed;
			$c_score=$link{$cluster}{$seed}{"score"};
			print "Link between $cluster and $seed -> $seed becomes a supercluster that includes $cluster -> $c_score\n";
		}
		elsif($link{$cluster}{$seed}{"score"}>$c_score){
			print "Oh wait, actually we switched $cluster to $seed which seems to be the more appropriate supercluster -> $link{$cluster}{$seed}{score}\n";
			$sc=$seed;
			$c_score=$link{$cluster}{$seed}{"score"};
		}
	}
	if ($tag==0){
		$sc=$cluster;
		push(@tab_seed,$cluster);

	}
	print $s3 "$cluster\t$sc\n";
	$cluster_to_supercluster{$cluster}=$sc;
	$store_supercluster{$sc}{"nb"}+=$store_clusters{$cluster}{"nb"};
	foreach my $prod (sort keys %{$store_clusters{$cluster}{"prod"}}){
		$store_supercluster{$sc}{"prod"}{$prod}+=$store_clusters{$cluster}{"prod"}{$prod};
	}
	foreach my $prot (sort keys %{$store_clusters{$cluster}{"prots"}}){
		$store_supercluster{$sc}{"prots"}{$prot}++;
	}
}
close $s3;

print "## Here are the superclusters \n";
my $out_file_supercluster=$root."_superclusters_composition.csv";
open my $s1,">",$out_file_supercluster;
print $s1 "PC,# members,predicted function(s),members list\n";
my @tab_fs;
foreach my $sc (sort {$store_supercluster{$b}{"nb"} <=> $store_supercluster{$a}{"nb"}  || $a cmp $b  } keys %store_supercluster){
	if ($store_supercluster{$sc}{"nb"}>1){push(@tab_fs,$sc);}
	my $line="";
	foreach my $prod (sort { $store_supercluster{$sc}{"prod"}{$b} <=> $store_supercluster{$sc}{"prod"}{$a} } keys %{$store_supercluster{$sc}{"prod"}}){
		my $prod_clean=$prod;
		$prod_clean=~s/,/ /g;
		chomp($prod_clean);
		$line.=$prod_clean." (".$store_supercluster{$sc}{"prod"}{$prod}.") ";
	}
	chop($line);
	$line.=",";
	foreach my $prot (sort keys %{$store_supercluster{$sc}{"prots"}}){
		$line.=$prot." ";
	}
	chop($line);
	print "$sc,$store_supercluster{$sc}{nb},$line\n";
	if ($store_supercluster{$sc}{"nb"}>1){
		print $s1 "$sc,$store_supercluster{$sc}{nb},$line\n";
	}
}
close $s1;



sub clean_ali_a3m{
	my $file=$_[0];
	my $cluster_name=$_[1];
	my $store;
	my $tag=0;
	open my $a3m,"<",$file;
	while(<$a3m>){
		chomp($_);
		if ($tag==0){
			$tag=1;
			$store=">".$cluster_name."\n";
		}
		else{
			$store.=$_."\n";
		}
	}
	close $a3m;
	open my $s1,">",$file;
	print $s1 $store;
	close $s1;


}

sub run_cmd{
	my $cmd=$_[0];
	my $out=`$cmd`;
	if ($_[1] ne "quiet" && $_[1] ne "veryquiet"){
		if ($_[1] eq "stderr"){print STDERR "$out\n";}
		else{print "$out\n";}
	}
	return($out);
}

sub test_avail{
	my $to_test=$_[0];
	my $path=&run_cmd("which ".$to_test);
	if ($path eq ""){die("pblm, we did not find the $to_test path, was the program installed and/or the right environment/module loaded ?\n");}
}
